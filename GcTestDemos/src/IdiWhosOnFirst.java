

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.WebDriver;

import common.BrowserDrivers;
import common.GutCheckLogin;
import common.Utilities;
import common.pageObjects.IdiModeratorPages;
import common.pageObjects.IdiRespondentPages;

/**
 * IdiWhosOnFirst.java is a demo of a moderator and a respondent 
 * having a chat. As Moderator, the demo sets up a new IDI and starts
 * recruiting. A Respondent comes into the Chat. Moderator and Respondent 
 * trade off on the lines from Abbot and Costello's "Who's on First?"
 */
public class IdiWhosOnFirst {

	public static WebDriver driver1;
	public static WebDriver driver2;
	public static String projectName = " ";
	public static Utilities utils1;
	public static Utilities utils2;
	public static IdiModeratorPages idiModPages;
	public static IdiRespondentPages idiRespPages;

	public static void main(String[] args) throws FileNotFoundException{

		driver1 = BrowserDrivers.startDriver("Firefox");
		utils1 = new Utilities(driver1);
		idiModPages = new IdiModeratorPages(driver1);
		utils1.setWindowSizeAndPosition(100, 0, 1100, 2000);
		// Moderator
		driver1.get("https://stage.gutcheckit.com/");
		utils1.wait(1000);
		GutCheckLogin.LoginAsAdmin(driver1);
		utils1.wait(1000);
		idiModPages.ClickCreateProject();
		Random rand = new Random();
		idiModPages.setProjectName("Abott & Costello #" + rand.nextInt(10000));
		idiModPages.startOneOnOne();
		utils1.wait(1000);
		idiModPages.clickInterviewPrep();
		idiModPages.addMedia("http://4.bp.blogspot.com/_OPbAxKeFvSg/TUl_o9pwkeI/" + 
				"AAAAAAAAA00/N_xFQBXr0lg/s1600/dvd+The+Abbott+and+Costello.jpg",
				"Abbot & Costello \"Who's on First\"");
		idiModPages.saveChatMedia();
		idiModPages.clickInterviewNow();
		idiModPages.startRecruitment();
		// Respondent
		driver2 = BrowserDrivers.startDriver("Chrome");
		utils2 = new Utilities(driver2);
		idiRespPages = new IdiRespondentPages(driver2);
		idiRespPages.navigateToSimulatedDashboard();
		utils2.setWindowSizeAndPosition(1360, 0, 1100, 1700);
		idiRespPages.clickProjectLink(projectName);
		idiRespPages.clickRespondentGetStarted();
		idiRespPages.clickRespondentContinue();
		// Moderator
		idiModPages.chatNowWithResopndent();
		//Message from Moderator to Respondent
		idiModPages.moderatorSendMessage("Let's begin...");	
		//Message from Respondent to Moderator
		idiRespPages.respondentSendMessage("OK");
		idiModPages.showMedia();
		idiRespPages.showMediaFullSize(3);
		parseWhosOnFirst();
		idiModPages.stopShowingMedia();
		idiModPages.endChat();
		// if save...
		idiModPages.saveInterview();
		idiModPages.rateInterview(3);
		
		if (driver2 != null){
			driver2.quit();
		}

		idiModPages.selectNextStep("guide");
		utils1.wait(1000);
		GutCheckLogin.Logout(driver1);
		utils1.wait(5000);
		if (driver1 != null){
			driver1.quit();
		}
	}

	
	public static void parseWhosOnFirst(){
		/**
		 * numberOfLinesToRun designates the total number of
		 * lines to be typed into chat.
		 */
		int numberOfLinesToRun = 1000;
		  try {
				File messageFile = new File("whos-on-first.txt");
				FileReader fileReader = new FileReader(messageFile);
				BufferedReader bufferedReader = new BufferedReader(fileReader);
				String line;
				int counter = 0; 
				while (((line = bufferedReader.readLine()) != null) && (counter < numberOfLinesToRun)) {
					if (line.contains("A b b o t t :")){
						String[] splitLine = line.split(":", 2);
						idiModPages.moderatorSendMessage(splitLine[1].trim());
					}
					else if (line.contains(" C o s t e l l o :")){
						String[] splitLine = line.split(":", 2);
						idiRespPages.respondentSendMessage(splitLine[1].trim());
					}
					else if (line.length() == 11){
						Thread.sleep(2000);
					}
					else {
						System.out.println(line);
					}
					counter++;
				}
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}		
	}
}
