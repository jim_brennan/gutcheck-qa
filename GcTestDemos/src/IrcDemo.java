

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common.BrowserDrivers;
import common.GutCheckLogin;
import common.RandomUtf8Text;
import common.Utilities;
import common.pageObjects.IrcModeratorPages;
import common.pageObjects.IrcRespondentPages;

public class IrcDemo {

	protected static String[] respondentNames0 = {"Mathew", "Ryan", "Betty",
		"Jerry", "Jamie", "Linda", "Mike","Danny", "Randall","Tina" };
	protected static String[] respondentNames1 = {"Roxanne", "Steven", "Consuela", "George","Larry",
		"Carl","Betty", "Rhonda", "Marilyn", "Billy",};
	protected static WebDriver modDriver;
	protected static WebDriver respDriver;
	protected static Utilities modUtils;
	protected static Utilities respUtils;
	protected static IrcModeratorPages modPages;
	protected static IrcRespondentPages respPages;
	protected Random rand = new Random();
	protected static String webSite = "http://";
	public static String respondentFirstName = "";
	public static String respondentLastInit = "";
	protected static int maxRespondents = 4;
	protected static int xPosition = 20;
	protected static int  yPosition = 50;
	protected static int  xDimension = 680;
	protected static int  yDimension = 360;
	protected static int respondentIndex = 0;

	@BeforeClass
	public static void classStartUp(){
		modDriver = BrowserDrivers.startDriver("Firefox");
		modUtils = new Utilities(modDriver);
		modPages = new IrcModeratorPages(modDriver);
		modDriver.get("https://stage.gutcheckit.com");
		GutCheckLogin.LoginAsAdmin(modDriver);
	}
	@AfterClass
	public static void classCleanUp(){
		modUtils.wait(5000);
		modDriver.quit();
		respDriver.quit();
	}
	@Test
	public void createBoard(){
		webSite = modPages.launchBoardWithDefaults();
		modPages.ClickCreateProject();
		modPages.setProjectName("Test Board " + rand.nextInt(10000));
		modPages.startCommunity();
		modPages.goToQuestionsPage();
		modDriver.findElements(By.className("x-btn-mc")).get(0).click();
		modUtils.wait(2000);
		modDriver.findElement(By.id("dayGrid1TextareaQuestion")).click();
		modUtils.wait(1000);
		modDriver.findElement(By.id("dayGrid1TextareaQuestion")).sendKeys(RandomUtf8Text.generateQuestion() + (char)13);
		modUtils.wait(1000);
		modPages.goToSettingsPage();
		modPages.setPartialSuccessUrl();
		modPages.setProjectFailUrl();
		modPages.setOverQuotaUrl();
		webSite = modPages.getIrcShortLink();
		modPages.launchIrc();
	}
	@Test
	public void startRespondentSession(){
		startRespSession("Safari", "MAC");
		respPages.clickJoinMyCommunity();
		respPages.enterRepondentFirstName(respondentFirstName);
		respPages.enterRespondentLastInitial();
		respPages.enterRespondentLocation();
		respPages.enterRespondentEmail();
		respPages.clickContinueWithoutFacebook();
		respPages.clickProceedToQuestion1();
		respPages.enterQuestionResponse();
		respPages.clickSubmit();
	}

	protected static void startRespSession(String browserName, String platform) {
		respondentFirstName = respondentNames0[respondentIndex];
		respondentIndex++;
		respDriver = common.BrowserDrivers.startDriver(browserName);
		respDriver.manage().deleteAllCookies();
		respUtils = new Utilities(respDriver);
		respPages = new IrcRespondentPages(respDriver);
		respUtils.wait(1000);
		respDriver.get(webSite);
		respUtils.wait(1000);
	}
}
