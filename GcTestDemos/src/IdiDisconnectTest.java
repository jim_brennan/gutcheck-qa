

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.BrowserDrivers;
import common.GutCheckLogin;
import common.RandomUtf8Text;
import common.Utilities;
import common.pageObjects.IdiModeratorPages;
import common.pageObjects.IdiRespondentPages;
/**
 * IDI Defect Demo
 */
public class IdiDisconnectTest {
	
	public static WebDriver driver1;
	public static WebDriver driver2;
	public static String projectName = "New One 0";
	public static Utilities utils1;
	public static Utilities utils2;
	public static IdiModeratorPages idiModPages;
	public static IdiRespondentPages idiRespPages;
	
	public static void main(String[] args){
		// Start-Up
		driver1 = BrowserDrivers.startDriver("firefox");
		driver1.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		utils1 = new Utilities(driver1);
		idiModPages = new IdiModeratorPages(driver1);
		utils1.setWindowSizeAndPosition(0, 0, 1100, 2000);

		// Moderator Starts
		driver1.get("https://stage.gutcheckit.com/");
		utils1.wait(3000);
		GutCheckLogin.LoginAsAdmin(driver1);
		utils1.wait(3000);
	    idiModPages.ClickCreateProject();
		Random rand = new Random();
		idiModPages.setProjectName("Launch IDI " + rand.nextInt(10000));
		idiModPages.startOneOnOne();
		driver1.findElement(By.linkText("INTERVIEW NOW")).click();
//		driver1.findElement(By.linkText(projectName)).click();
//		idiModPages.manageProjectChatNow();
		idiModPages.startRecruitment();
		
		// Wait for the IDI to start recruiting
		boolean ready = false;
		int count = 0;
		while (!ready && (count < 60)){
			List<WebElement> elements1 = driver1.findElements(By.className("green-small-button"));
			if (elements1.size() > 0){
				ready = true;
			}
			else{
				utils1.wait(1000);
				count++;
			}
		}

		// Respondent Starts
		driver2 = BrowserDrivers.startDriver("Safari");
		driver2.manage().deleteAllCookies();
		utils2 = new Utilities(driver2);
		idiRespPages = new IdiRespondentPages(driver2);
		idiRespPages.navigateToSimulatedDashboard();
		utils2.setWindowSizeAndPosition(1110, 0, 1100, 1700);
		List<WebElement> elements = driver2.findElements(By.partialLinkText(projectName));
		elements.get(elements.size() - 1).click();
		//idiRespPages.clickProjectLink(projectName);
		utils1.wait(3000);
		idiRespPages.clickRespondentGetStarted();
		idiRespPages.clickRespondentContinue();
		utils2.wait(4000);	
		
		// Moderator
		idiModPages.chatNowWithResopndent();
		
		//Message from Moderator to Respondent
		idiModPages.moderatorSendMessage("Let's begin...");	
		
		//Message from Respondent to Moderator
		idiRespPages.respondentSendMessage("OK");
		
		 Thread t = new Thread(new RespondentSim());
	     t.start();	
	     
	     for (int i = 0; i < 300; i++){
	    	
			idiModPages.moderatorSendMessage(RandomUtf8Text.generateText(rand.nextInt(150)));	
			List<WebElement> elements2 = driver1.findElements(By.id("notify-container"));
			SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
			if (elements2.size() > 0 && !elements2.get(0).getText().isEmpty()){
				System.out.println(elements2.get(0).getText());
				Calendar cal = Calendar.getInstance();
				System.out.println(dateFormat.format(cal.getTime()));
			}
			//utils2.wait(rand.nextInt(5000));
	     }
		
		driver1.quit();
		driver2.quit();
	}

	 public static class RespondentSim implements Runnable {
	
     public void run() {
    	 	Random rand = new Random();
    		for (int i = 0; i < 100; i++){
    			// Message from respondent
    			utils2.wait(500);
    			driver2.findElement(By.id("chatInputTextArea")).clear();
    			for (int j = 0; j < 40; j++){
    				driver2.findElement(By.id("chatInputTextArea")).sendKeys(RandomUtf8Text.generateText(rand.nextInt(7)));
    				//utils2.wait(rand.nextInt(500));
    			}
    			driver2.findElement(By.cssSelector("div.orange-big-button")).click();
    			utils2.wait(500);
    		}
       
	    }
	 }


}
