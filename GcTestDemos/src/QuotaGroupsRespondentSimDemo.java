

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import common.BrowserDrivers;
import common.RandomNames;
import common.SpreadsheetData;
import common.Utilities;
import common.pageObjects.IrcRespondentPages;

public class QuotaGroupsRespondentSimDemo {

	public WebDriver driver;
	public Utilities utils;
	public IrcRespondentPages respPages;
	public static Random rand;

	public String expectedResult;
	public String respondentAge;
	public String gender;
	public String employment;
	public String householdIncome;
	public String education;
	public String zipCode;
	public String ethnicity;
	public String maritalStatus;
	public String children;
	public String childsGender;
	public String childsAge;
	public String secondChildsGender;
	public String secondChildsAge;
	public String question1;
	public String question2;
	//public String projectLink = "http://goo.gl/a2ezZA";

	@DataProvider(name = "dp")
	public static Iterator<Object[]> spreadsheetData() throws IOException {
		InputStream spreadsheet = new FileInputStream(
				"data//QuotaGroupDemo.xls");
		Iterator<Object[]> objectArray = new SpreadsheetData(spreadsheet)
		.getData().iterator();
		return objectArray;
	}
	/*
	 * These are the screening data mapped horizontally on the data sheet.  
	 */
	@Factory(dataProvider = "dp")
	public QuotaGroupsRespondentSimDemo(
			String expectedResult,
			String respondentAge,
			String gender,
			String employment,
			String householdIncome,
			String education,
			String zipCode,
			String ethnicity,
			String maritalStatus,
			String children,
			String childsGender,
			String childsAge,
			String secondChildsGender,
			String secondChildsAge,
			String question1,
			String question2)
	{
		this.expectedResult = expectedResult;
		this.respondentAge =  respondentAge;
		this.gender = gender;
		this.employment = employment;
		this.householdIncome = householdIncome;
		this.education = education;
		this.zipCode = zipCode;
		this.ethnicity = ethnicity;
		this.maritalStatus = maritalStatus;	
		this.children = children;
		this.childsGender = childsGender;
		this.childsAge = childsAge;
		this.secondChildsGender = secondChildsGender;
		this.secondChildsAge = secondChildsAge;
		this.question1 = question1;
		this.question2 = question2;
	}


	@Test
	public  void joinIrc(){
		driver = BrowserDrivers.startDriver("Chrome");
		utils = new Utilities(driver);
		respPages = new IrcRespondentPages(driver);

		/* Access through Simulated Dashboard */
		driver.manage().deleteAllCookies();
		utils.wait(1000);
		driver.get("https://dev.gutcheckit.com/respondent/dashboard.jsp");
		utils.wait(2000);
		respPages.clickProjectLink("Cascade - Pacs Design V", 8);
		utils.wait(500);

		/* Access with Project Link  */
		//		driver.get(projectLink);
		//		utils.wait(4000);

		if (!driver.getCurrentUrl().contains("overQuota")){


			respPages.clickJoinMyCommunity();

			//******************** Login Page **************
			String name = RandomNames.getRandomFirstName();
			respPages.enterRepondentFirstName(name);
			respPages.enterRespondentLastInitial(name.substring(0, 1));
			respPages.enterRespondentLocation(name + ", CA");
			respPages.enterRespondentEmail();
			respPages.pickRandomAvatar();
			respPages.clickContinueWithoutFacebook();
			utils.wait(500);
			
			//******************** Screening Page **************	
			if (checkforDemoField("ageField")){
				respPages.selectRespondentAge(respondentAge);
			}	
			utils.wait(500);
			if (checkforDemoField("genderGroup")){
				respPages.selectRespondentGender(gender);
			}
			if (checkforDemoField("incomeGroup")){
				respPages.selectRespondentIncome(householdIncome);
			}
			if (checkforDemoField("educationGroup")){
				respPages.selectRespondentEducation(education);
			}
			if (checkforDemoField("ethnicityGroup")){
				respPages.selectRespondentEthnicity(ethnicity);
			}
			if (checkforDemoField("maritalStatusGroup")){
				respPages.selectRespondentMaritalStatus(maritalStatus);
			}
			if (checkforDemoField("zipCodeField")){
				respPages.selectRespondentZipCode(zipCode);
			}
			if (checkforDemoField("employmentGroup")){
				respPages.selectRespondentEmployment(employment);
			}
			if (checkforDemoField("childrenGroup")){
				respPages.selectRespondentChildren(children, childsAge, childsGender);
			}
			if (checkforDemoField("secondChildGroup")){
				respPages.addSecondRespondentChild(secondChildsAge, secondChildsGender);
			}

			utils.wait(1000);
			respPages.screeningPageClickNext();
			utils.wait(2000);

				//******************** Custom Question 1 ************** 
				if (driver.getCurrentUrl().contains("gutcheckit")){
					if (!question1.contentEquals("0")){
						respPages.selectRadioOrCheckBoxValues(question1);
						respPages.screeningPageClickNext();
						utils.wait(1000);	
					}
				}

				//		//******************** Custom Question 2 ************** 
				if (driver.getCurrentUrl().contains("gutcheckit")){
					if (!question2.contentEquals("0")){
						respPages.selectRadioOrCheckBoxValues(question2);
						respPages.screeningPageClickNext();
						utils.wait(1500);
					}
				} 

			if (driver.getCurrentUrl().contains("gutcheckit")){
				respPages.clickProceedToQuestion1();
				boolean finished = false;
				while (!finished){
					utils.wait(500);
					List<WebElement> pollAnswers = driver.findElements(By.className("x-form-radio"));
					if (pollAnswers.size() > 0) {
						rand = new Random();
						pollAnswers.get(rand.nextInt(4)).click();
					}
					utils.wait(500);
					//respPages.enterQuestionResponse("english");
					respPages.enterQuestionResponseText("I am a " + respondentAge +
							"-year-old " + ethnicity + " " + gender + ", " + employment + ", $" + householdIncome +
							", " + maritalStatus + ", " + education + " Custom 1: " + question1 + " Custom 2: " + question2);
					respPages.clickSubmit();
					utils.wait(3000);

					if (driver.findElements(By.className("orange-big-button")).size() > 0){
						utils.wait(500);
						driver.findElement(By.className("orange-big-button")).click();
						utils.wait(1000);
					}
					else{
						finished = true;
						break;
					} 
				}

			}
			utils.wait(500);
			driver.quit();
			utils.wait(1000);
		}


	}
	private Boolean checkforDemoField(String demoField){
		Boolean isItThere = false;
		List<WebElement> elements = driver.findElements(By.className("x-form-text"));
		for (WebElement e : elements){
			if (e.getAttribute("name").contentEquals(demoField)){
				isItThere = true;
				break;
			}
		}
		return isItThere;
	}
}
