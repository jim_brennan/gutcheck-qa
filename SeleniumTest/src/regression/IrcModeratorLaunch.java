package regression;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common.GutCheckLogin;
import common.GutCheckTestBase;
import common.pageObjects.IrcModeratorPages;

public class IrcModeratorLaunch extends GutCheckTestBase{
	
	protected static IrcModeratorPages modPages;
	
	@BeforeClass
	public static void classStartUp(){
		modPages = new IrcModeratorPages(driver);
		GutCheckLogin.LoginAsAdmin(driver);
	}

	@Test
	public void launchIrc(){
		modPages.launchBoardWithDefaults();
		utils.wait(3000);
		Assert.assertTrue(driver.findElement(By.className("x-panel-body")).getText().contains("Project: Test Board"));
	}
}










