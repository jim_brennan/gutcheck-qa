package regression;

import java.util.Random;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common.BrowserDrivers;
import common.GutCheckLogin;
import common.GutCheckTestBase;
import common.RemoteBrowserDrivers;
import common.Utilities;
import common.pageObjects.IrcModeratorPages;
import common.pageObjects.IrcRespondentPages;

public class IrcBoardQuota extends GutCheckTestBase{

	protected static IrcModeratorPages modPages;
	protected static IrcRespondentPages respPages;
	protected static String webSite = "http://stage.gutcheckit.com/";
	protected static String projectName;
	protected static String projectLink;
	protected static String[] respondentNames = { "Mathew", "Ryan", "Betty",
		"Jerry", "Jamie", "Linda", "Mike", "Danny", "Randall", "Tina",
		"Roxanne", "Steven", "Consuela", "George", "Larry", "Carl",
		"Betty", "Rhonda", "Marilyn", "Billy" };
	public static String respondentFirstName = "";
	protected static Random rand = new Random();

	@BeforeClass
	public static void localClassStartUp(){
		modPages = new IrcModeratorPages(driver);
		driver.get(webSite);
		GutCheckLogin.LoginAsAdmin(driver);
		modPages.createBoardWithDefaults();
		modPages.goToSettingsPage();
		modPages.setClickCount(2);
		modPages.setDiscussionDays(1);
		projectLink = modPages.getIrcShortLink();
		modPages.launchIrc();
		utils.wait(500);
		driver.quit();
		utils.wait(3000);
	}

	@Test
	public void respondToIrc1() {
		startRespSession(1);
		respPages.clickJoinMyCommunity();
		respPages.enterRepondentFirstName(respondentFirstName);
		respPages.enterRespondentLastInitial();
		respPages.enterRespondentLocation();
		respPages.enterRespondentEmail();
		respPages.clickContinueWithoutFacebook();
		respPages.clickProceedToQuestion1();
		respPages.enterQuestionResponse();
		respPages.clickSubmit();
		utils.wait(3000);
		Assert.assertTrue(driver
				.findElement(By.className("orange-big-nobutton")).getText()
				.contains("Thank you for completing all of today's questions."));
	}
	@Test
	public void respondToIrc2() {
		startRespSession(2);
		respPages.clickJoinMyCommunity();
		respPages.enterRepondentFirstName(respondentFirstName);
		respPages.enterRespondentLastInitial();
		respPages.enterRespondentLocation();
		respPages.enterRespondentEmail();
		respPages.clickContinueWithoutFacebook();
		respPages.clickProceedToQuestion1();
		respPages.enterQuestionResponse();
		respPages.clickSubmit();
		utils.wait(1000);
		Assert.assertTrue(driver
				.findElement(By.className("orange-big-nobutton")).getText()
				.contains("Thank you for completing all of today's questions."));
	}
	@Test
	public void respondToIrc3() {
		startRespSession(7);
		utils.wait(2000);
		Assert.assertTrue(driver.getCurrentUrl().contains("overQuota"));
//		int timeout = 60;
//		int timer = 0;
//		boolean found = false;
//		while ((timer < timeout) && (!found)){
//			List<WebElement> elements = driver.findElements(By.className("title"));
//			if (elements.size() > 0){
//				found = true;
//				break;
//			}else{
//				utils.wait(1000);
//				timer++;
//			}
//		}
//		
//		Assert.assertTrue(driver.findElement(By.className("title")).getText().contains("can't"));
	}
	
	@AfterMethod
	public static void tearDown(){
		if (driver != null){
		driver.manage().deleteAllCookies();
		}
		utils.wait(1000);
		driver.quit();
		utils.wait(1000);
	}
	public static void startRespSession(int respondentNum) {
		if (serverLocation.contains("remote")) {
			driver = RemoteBrowserDrivers.startDriver(browserName, platform);
			driver.manage().deleteAllCookies();
		} else {
			driver = BrowserDrivers.startDriver(browserName);
			driver.manage().deleteAllCookies();
		}
		respondentFirstName = respondentNames[respondentNum];
		utils = new Utilities(driver);
		utils.wait(1000);
		respPages = new IrcRespondentPages(driver);
		driver.manage().deleteAllCookies();
		driver.get(projectLink);
		utils.wait(1000);
	}

}

