package regression;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

import org.openqa.selenium.By;
import org.testng.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common.BrowserDrivers;
import common.GutCheckLogin;
import common.GutCheckTestBase;
import common.DateMover;
import common.RemoteBrowserDrivers;
import common.pageObjects.IrcModeratorPages;

public class AutoLaunchBoard extends GutCheckTestBase{

	protected static IrcModeratorPages modPages;
	protected static Random rand = new Random();
	protected static String projectName;
	public static Calendar cal = Calendar.getInstance();
	protected static String autoLaunchDate; 
/**
 * This test creates a board, then "ages" it a day-at-a-time, first
 * by changing the auto-launch date and running the auto-launch tool,
 * then by moving back the start date of the first board topic. 
 * 
 * The tests verify that as the Board ages, the appropriate questions
 * are enabled automatically.
 */
	@BeforeClass
	public static void localClassStartUp(){
		int discussionDays = 5;
		int numberOfQuestions = 1;
		modPages = new IrcModeratorPages(driver);
		GutCheckLogin.LoginAsAdmin(driver);
		utils.wait(3000);
		cal.add(Calendar.HOUR, +24);
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
		autoLaunchDate = timeFormat.format(cal.getTime());   
		projectName = modPages.createBoardWithDefaults();
		modPages.goToSettingsPage();
		utils.wait(2000);
		//modPages.changeToLink();
		modPages.setDiscussionDays(discussionDays);
		modPages.goToQuestionsPage();
		utils.wait(3000);
		for (int i = 0; i < discussionDays; i++){
			for (int j = 0; j < numberOfQuestions; j++){
				modPages.enterQuestion(i + 1);
				// Add Poll
				utils.wait(500);
//				modPages.clickAddPoll();
//				utils.wait(1000);
//				modPages.clickAddAnswer();
//				modPages.enterAnswer("This is my answer 1...");
//				modPages.clickSavePollAnswer();modPages.clickAddAnswer();
//				modPages.enterAnswer("This is my answer 2...");
//				modPages.clickSavePollAnswer();
//				modPages.clickAddAnswer();
//				modPages.enterAnswer("This is my answer 3...");
//				modPages.clickSavePollAnswer();
//				modPages.clickPollOk();
//				utils.wait(500);
			}
			
		}
		modPages.goToSettingsPage();
		utils.wait(600);
		modPages.checkIrcAutoLaunch();
		modPages.setIrcAutoLaunchDate(autoLaunchDate);
		utils.wait(1000);
	}
	@AfterClass
	public static void tearDown(){
		driver.quit();
	}
	/**
	 * Moves the start date back a day (to today), runs the auto-launch utility
	 * and verifies that the first day's questions are live.
	 */
	@Test (priority=1) 
	public void resetAutoLaunchToStartBoard() throws ParseException{ 
		DateMover.moveBackAutoLaunchDate(projectName, 1);
		driver.findElement(By.linkText("Administration")).click();
		driver.findElement(By.linkText("Run the project auto-launch job")).click();
		utils.wait(1000);
		driver.quit();
		utils.wait(1000);
		startDriver();
		Assert.assertTrue(driver.findElement(By.id("dayGrid1")).getText().contains("0 answers"));
	}
	/**
	 * The rest of the tests continue moving the date 
	 * and validating the questions for the next day.
	 */
	@Test (dependsOnMethods="resetAutoLaunchToStartBoard")
	public void Day1ToDay2() throws ParseException{
		ageIrcBoard(1);
		Assert.assertTrue(driver.findElement(By.id("dayGrid2")).getText().contains("0 answers"));
	}
	@Test  (dependsOnMethods="Day1ToDay2")
	public void Day2ToDay3() throws ParseException{
		ageIrcBoard(1); 
		Assert.assertTrue(driver.findElement(By.id("dayGrid3")).getText().contains("0 answers"));
	}
	@Test  (dependsOnMethods="Day2ToDay3")
	public void Day3ToDay4() throws ParseException{
		ageIrcBoard(1);
		Assert.assertTrue(driver.findElement(By.id("dayGrid4")).getText().contains("0 answers"));
	}
	@Test   (dependsOnMethods="Day3ToDay4")
	public void day4ToDay5() throws ParseException{
		ageIrcBoard(1);
		Assert.assertTrue(driver.findElement(By.id("dayGrid5")).getText().contains("0 answers"));
	}

	public void ageIrcBoard(int numberOfDays) throws ParseException{
		DateMover.moveInitialBoardTopicDate(projectName,numberOfDays);
		driver.findElement(By.linkText("Administration")).click();
		driver.findElement(By.linkText("Run the project auto-launch job")).click();
		utils.wait(500);
		driver.quit();
		utils.wait(500);
		startDriver();
	}
	public static void startDriver(){
		if (serverLocation.toUpperCase().contains("REMOTE")){
			driver = RemoteBrowserDrivers.startDriver(browserName, platform);
		}
		else{
			driver = BrowserDrivers.startDriver(browserName);
		}
		modPages = new IrcModeratorPages(driver);
		driver.get(webSite);
		GutCheckLogin.LoginAsAdmin(driver);
		utils.wait(2000);
		driver.findElement(By.linkText(projectName)).click();
		utils.wait(5000);
	}
}

