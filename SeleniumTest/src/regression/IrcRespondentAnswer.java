package regression;

import java.util.Random;

import org.testng.Assert;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common.BrowserDrivers;
import common.GutCheckLogin;
import common.GutCheckTestBase;
import common.RemoteBrowserDrivers;
import common.Utilities;
import common.pageObjects.IrcModeratorPages;
import common.pageObjects.IrcRespondentPages;

public class IrcRespondentAnswer extends GutCheckTestBase {

	protected static String[] respondentNames = { "Mathew", "Ryan", "Betty",
			"Jerry", "Jamie", "Linda", "Mike", "Danny", "Randall", "Tina",
			"Roxanne", "Steven", "Consuela", "George", "Larry", "Carl",
			"Betty", "Rhonda", "Marilyn", "Billy" };
	protected static IrcModeratorPages modPages;
	protected static IrcRespondentPages respPages;
	protected static Random rand = new Random();
	protected static String webSite = "";
	public static String respondentFirstName = "";

	@BeforeClass
	public static void classStartUp() {
		modPages = new IrcModeratorPages(driver);
		utils.wait(1500);
		GutCheckLogin.LoginAsAdmin(driver);
		webSite = modPages.launchBoardWithDefaults();
		utils.wait(3000);
		driver.quit();
	}

	@Test
	public void respondToIrc() {
		startRespSession();
		utils.wait(1500);
		respPages.clickJoinMyCommunity();
		respPages.enterRepondentFirstName(respondentFirstName);
		respPages.enterRespondentLastInitial();
		respPages.enterRespondentLocation();
		respPages.enterRespondentEmail();
		respPages.clickContinueWithoutFacebook();
		respPages.clickProceedToQuestion1();
		respPages.enterQuestionResponse();
		respPages.clickSubmit();
		utils.wait(3000);
		Assert.assertTrue(driver
				.findElement(By.className("orange-big-nobutton")).getText()
				.contains("Thank you for completing all of today's questions."));
	}

	public static void startRespSession() {
		if (serverLocation.contentEquals("remote")) {
			driver = RemoteBrowserDrivers.startDriver(browserName, platform);
			//driver.manage().deleteAllCookies();
		} else {
			driver = BrowserDrivers.startDriver(browserName);
			//driver.manage().deleteAllCookies();
		}
		respondentFirstName = respondentNames[rand
				.nextInt(respondentNames.length)];
		utils.wait(1000);
		utils = new Utilities(driver);
		respPages = new IrcRespondentPages(driver);
		driver.get(webSite);
		utils.wait(1000);
	}
}
