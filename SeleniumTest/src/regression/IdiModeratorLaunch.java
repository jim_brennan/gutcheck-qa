package regression;

import java.util.Random;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common.GutCheckLogin;
import common.GutCheckTestBase;
import common.pageObjects.IdiModeratorPages;

/**
 * IdiModeratorLaunch
 */
public class IdiModeratorLaunch extends GutCheckTestBase {

	public String projectName = " ";
	public IdiModeratorPages idiModPages;

	@BeforeClass
	public void ClassSeUp() {
		idiModPages = new IdiModeratorPages(driver);
		utils.setWindowSizeAndPosition(0, 0, 1100, 2000);
	}

	@Test
	public void startIdi() {

		// Moderator
		GutCheckLogin.LoginAsAdmin(driver);
		utils.wait(3000);
		idiModPages.ClickCreateProject();
		Random rand = new Random();
		idiModPages.setProjectName("LaunchIdi" + rand.nextInt(10000));
		idiModPages.startOneOnOne();
		idiModPages.manageProjectChatNow();
		idiModPages.startRecruitment();

		utils.wait(2000);
		Assert.assertTrue(driver
				.findElement(By.id("recruitmentNotificationStatus")).getText()
				.contains("Searching"));
		utils.wait(2000);
	}

}
