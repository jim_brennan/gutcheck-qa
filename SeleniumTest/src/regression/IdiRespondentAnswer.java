package regression;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.BrowserDrivers;
import common.GutCheckLogin;
import common.RemoteBrowserDrivers;
import common.Utilities;
import common.pageObjects.IdiModeratorPages;
import common.pageObjects.IdiRespondentPages;

/**
 * IdiRespondentAnswer
 */
public class IdiRespondentAnswer{

	public String projectName = " ";
	public IdiModeratorPages idiModPages;
	public IdiRespondentPages idiRespPages;
	public Utilities moderatorUtils;
	public Utilities respondentUtils;
	public WebDriver moderatorDriver;
	public WebDriver respondentDriver;
	public String moderatorBrowserName;
	public String respondentBrowserName;
	public String serverLocation;
	public String platform;
	public String webSite = "http://stage.gutcheckit.com/";
	
	@BeforeClass
	@Parameters({ "serverLocation", "browserName", "platform" })
	public void startSession(String serverLocation, String browserName,
			String platform) {
		respondentBrowserName = browserName;
		this.serverLocation = serverLocation;
		this.platform = platform;

		if (respondentBrowserName.toUpperCase().contains("FIREFOX")){
			moderatorDriver = startBrowserDriver("chrome");
		}
		else{
			moderatorDriver = startBrowserDriver("firefox");
		}
		idiModPages = new IdiModeratorPages(moderatorDriver);
		moderatorUtils = new Utilities(moderatorDriver);
		moderatorDriver.get(webSite);
		// Moderator Launches IDI
		GutCheckLogin.LoginAsAdmin(moderatorDriver);
		moderatorUtils.wait(3000);
		idiModPages.ClickCreateProject();
		Random rand = new Random();
		idiModPages.setProjectName("LaunchIdi" + rand.nextInt(10000));
		idiModPages.startOneOnOne();
		idiModPages.manageProjectChatNow();
		idiModPages.startRecruitment();
	}
	@AfterClass
	public void classTearDown(){
		respondentUtils.wait(2000);	
		moderatorDriver.quit();
		respondentUtils.wait(2000);	
		respondentDriver.quit();
		
	}

	@Test
	public void startIdi() {
		// Respondent joins IDI Chat
		respondentDriver = startBrowserDriver(respondentBrowserName);
		respondentUtils = new Utilities(respondentDriver);
		idiRespPages = new IdiRespondentPages(respondentDriver);
		idiRespPages.navigateToSimulatedDashboard();
		idiRespPages.clickProjectLink(projectName);
		idiRespPages.clickRespondentGetStarted();
		idiRespPages.clickRespondentContinue();
		respondentUtils.wait(4000);	

		// Moderator
		idiModPages.chatNowWithResopndent();

		//Message from Moderator to Respondent
		idiModPages.moderatorSendMessage("Let's begin...");	

		//Message from Respondent to Moderator
		idiRespPages.respondentSendMessage("OK");
		respondentUtils.wait(2000);	
		Assert.assertTrue(respondentDriver.findElement(By.id("chatDataAreaRespondent")).getText().contains("Let's begin..."));

	}

	public WebDriver startBrowserDriver(String browserName){
		WebDriver driver;
		if (serverLocation.contentEquals("remote")) {
			driver = RemoteBrowserDrivers.startDriver(browserName, platform);
		} 
		else {
			driver = BrowserDrivers.startDriver(browserName);
		}
		return driver;
	}
}
