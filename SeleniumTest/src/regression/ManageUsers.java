package regression;

import java.util.List;

import org.testng.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import common.GutCheckLogin;
import common.GutCheckTestBase;
import common.RandomNames;

public class ManageUsers extends GutCheckTestBase {
	
	public String displayName = "";
	String email = "";

	@Test
	public void registerUser(){
		String firstName = RandomNames.getRandomFirstName();
		String lastName = RandomNames.getRandomLastName();
		email = firstName.toLowerCase() + lastName.substring(0, 1).toLowerCase() + "@yahoo.com";

		driver.get("https://stage.gutcheckit.com/registration/?allowRegistration=yes");
		utils.wait(1000);
		driver.findElement(By.id("firstName")).clear();
		driver.findElement(By.id("firstName")).sendKeys(firstName);
		driver.findElement(By.id("lastName")).clear();
		driver.findElement(By.id("lastName")).sendKeys(lastName);
		displayName = firstName + lastName.charAt(0);
		driver.findElement(By.id("displayName")).clear();
		driver.findElement(By.id("displayName")).sendKeys(displayName);
		driver.findElement(By.id("email1")).clear();
		driver.findElement(By.id("email1")).sendKeys(email);
		driver.findElement(By.id("password1")).clear();
		driver.findElement(By.id("password1")).sendKeys("tester2020");
		driver.findElement(By.id("termCondition")).click();
		utils.wait(500);
		utils.selectFromClassByAttribute("x-btn-text","text", "CREATE MY ACCOUNT").click();
		utils.wait(1000);
		utils.selectFromClassByAttribute("x-btn-text", "text","OK");
		utils.wait(500);
		utils.selectFromClassByAttribute("x-btn-text","text", "CREATE MY ACCOUNT").click();
		GutCheckLogin.Login(driver, email, "tester2020");
		Assert.assertTrue(driver.findElement(By.linkText(email)) != null);
		utils.wait(500);
		GutCheckLogin.Logout(driver);
		utils.wait(500);
	}
	@Test
	public void searchUsers(){
		GutCheckLogin.LoginAsAdmin(driver);
		clickAdminTab();
		clickManageUsers();
		enterTextSearchField(displayName);
		utils.wait(1000);
		Assert.assertTrue(matchSearchResult(displayName));
	}

	public void clickAdminTab() {
		driver.findElement(By.cssSelector("img[alt=\"Administration\"]")).click();
		utils.wait(500);
	}
	public void clickManageUsers(){
		driver.findElement(By.linkText("Manage Users")).click();
		utils.wait(500);
	}
	public void clickUserInformationTab(){
		utils.selectFromClassByAttribute("x-tab-strip-text" , "text", "User Information");
	}
	public void clickCompanyInformationTab(){
		utils.selectFromClassByAttribute("x-tab-strip-text" , "text", "Company Information");
	}
	public void enterTextSearchField(String searchString){
		driver.findElement(By.id("pattern")).clear();
		driver.findElement(By.id("pattern")).sendKeys(searchString + Keys.RETURN);
		utils.wait(500);
	}
	public boolean matchSearchResult(String resultToMatch){
		boolean resultFound = false;
		List<WebElement> elements = driver.findElements(By.className("x-grid3-cell-inner"));
		for (WebElement e : elements){
			if (e.getText().contains(resultToMatch)){
				resultFound = true;
			}
		}
		return resultFound;

	}
}





