package common;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
/**
 * BrowserDrivers contains methods to launch IE, Firefox, Chrome and Safari browsers. 
 * @author jbrennan
 *
 */
public class BrowserDrivers {

	public static WebDriver driver;
	protected static DesiredCapabilities capabilities;
/**
 * 
 * @param browserName "Internet Explorer", "Firefox", "Chrome" or "Safari"
 * @return WebDriver instance for new browser
 */
	public static WebDriver startDriver(String browserName) {
		if (browserName.contains("explore")) {
			capabilities = new DesiredCapabilities(
					DesiredCapabilities.internetExplorer());
			capabilities.setCapability("EnableNativeEvents", true);
			driver = new InternetExplorerDriver(capabilities);
			
		} else if (browserName.toUpperCase().contains("FIREFOX")) {
			File profileFile = new File(
					"//Users//jbrennan//Library//Application Support//Firefox//Profiles//9nxvm72u.AutoTester");
			File firefoxBinaryFile = new File(
					"/Applications/Firefox.app/Contents/MacOS/firefox-bin");
			FirefoxBinary fb = new FirefoxBinary(firefoxBinaryFile);
			FirefoxProfile profile = new FirefoxProfile(profileFile);
			profile.setPreference( "extensions.checkCompatibility", "false"); 
			profile.setAcceptUntrustedCertificates(true);
			profile.setEnableNativeEvents(true);
			driver = new FirefoxDriver(fb, profile);
			
		} else if (browserName.toUpperCase().contains("CHROME")) {
			capabilities = new DesiredCapabilities(DesiredCapabilities.chrome());
			capabilities.setVersion("14");
			capabilities.setBrowserName(browserName);
			driver = new ChromeDriver(capabilities);
			
		} else if (browserName.toUpperCase().contains("SAFARI")) {
			capabilities = new DesiredCapabilities(DesiredCapabilities.safari());
			capabilities.setBrowserName("Safari");
			capabilities.setJavascriptEnabled(true);
			driver = new SafariDriver(capabilities);
			
		} else {
			driver = new HtmlUnitDriver();
		}
		return driver;
	}

	public static void closeSession() {
		if (driver != null) {
			driver.quit();
		}
	}

}
