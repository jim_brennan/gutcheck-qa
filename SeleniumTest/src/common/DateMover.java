package common;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;



public class DateMover {

	public static void moveBackAutoLaunchDate(String projectName, int numberOfDays) {

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -numberOfDays);
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
		String autoLaunchDate = timeFormat.format(cal.getTime());            

		String autoLaunchDateQuery =  
				"Update board_setting set auto_launch_date = '" + autoLaunchDate + "' where project_id = " +
						"(SELECT project_id FROM project where project_name = '" + projectName + "');";

		DbQuery.executeDb(autoLaunchDateQuery);

		Utilities.staticWait(1000);
	} 
	
	public static void nullifyLastContactTime(String projectName){
		String getProjectIdQuery = 
				"Update project_user set last_contact_time = null where project_id = " +
				"(SELECT project_id FROM project where project_name = '" + projectName + "');";
		DbQuery.executeDb(getProjectIdQuery);
	    
		Utilities.staticWait(1000);
	}

	public static void moveInitialBoardTopicDate(String projectName, int numberOfDays) throws ParseException{
		
		
		String currentStartTimeQuery = 
		"Select start_date from board_topic where board_topic_id = (Select MIN(board_topic_id) from board_topic" + 
		" where start_date = (Select MIN(start_date) from board_topic where project_id = " +
		"(SELECT project_id FROM project where project_name = '" + projectName + "')))";
	    List<String> results = DbQuery.queryDb(currentStartTimeQuery);
	    String currentStartTime = results.get(0);
	    
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		cal.setTime(timeFormat.parse(currentStartTime));
		
		for (int counter = 0; counter < numberOfDays; counter++){
			cal.add(Calendar.HOUR, -24);
		}
		
		String newStart = timeFormat.format(cal.getTime());            

		String startTimeQuery =  
				"Update board_topic set start_date = '" + newStart + "' where board_topic_id = " + 
		"(Select MIN(board_topic_id) from board_topic" + 
		" where start_date = (Select MIN(start_date) from board_topic where project_id = " +
		"(SELECT project_id FROM project where project_name = '" + projectName + "')))";
	
		DbQuery.executeDb(startTimeQuery);

		Utilities.staticWait(1000);
	}
}





