package common;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class RandomUtf8Text {

	public static String generateQuestion()   {
		Random rnd = new Random();
		String testString = generateText(rnd.nextInt(400) + 160, "english");
		String questionString = testString + "?";
		return questionString;	
	}
	public static String generateQuestion(String language)  {
		Random rnd = new Random();
		String testString = "";
		while (testString.length() < 20){
			testString = generateText(rnd.nextInt(400) + 300, language);
		}
		String questionString = testString + "?";
		
		return questionString;	
	}
	public static String generateText(int charLength) {
		return generateText(charLength, "english");
	}

	public static String generateText(int charLength, String language) {
		String charSet = "UTF-8";
		Random rnd = new Random();
		List<String> textFileNames = new ArrayList<String>();

		if (language.toLowerCase().contains("english")){

			textFileNames.add("crime");
			textFileNames.add("sense_and_sensibility");
			textFileNames.add("pride_and_prejudice");
			charSet = "UTF-8";
			
		}else if (language.toLowerCase().contains("german")){

			textFileNames.add("german_text");
			textFileNames.add("german_text2");
			charSet = "UTF-8";

		}else if (language.toLowerCase().contains("japanese")){

			textFileNames.add("japanese_text");
			textFileNames.add("japanese_text2");
			charSet = "UTF-8";

		}else if (language.toLowerCase().contains("chinese")){
			textFileNames.add("chinese_text");
			textFileNames.add("chinese_text2");
			charSet = "UTF-8";

		}else if (language.toLowerCase().contains("spanish")){
			textFileNames.add("spanish_text");
			textFileNames.add("spanish_text1");
			charSet = "UTF-8";
			
		}else if (language.toLowerCase().contains("hindi")){
			textFileNames.add("hindi_text");
			textFileNames.add("hindi_text1");
			charSet = "UTF-8";
		}
		else if (language.toLowerCase().contains("thai")){
			textFileNames.add("thai_text");
			textFileNames.add("thai_text1");
			charSet = "UTF-16";
		}
		else if (language.toLowerCase().contains("filipino")){
			textFileNames.add("tagalog_text");
			textFileNames.add("tagalog_text1");
			charSet = "UTF-8";
		}
		else if (language.toLowerCase().contains("vietnamese")){
			textFileNames.add("vietnamese_text");
			textFileNames.add("vietnamese_text1");
			charSet = "UTF-16";
		}
		else if (language.toLowerCase().contains("korean")){
			textFileNames.add("korean_text");
			textFileNames.add("korean_text1");
			charSet = "UTF-16";
		}
		else if (language.toLowerCase().contains("turkish")){
			textFileNames.add("turkish_text");
			textFileNames.add("turkish_text1");
			charSet = "UTF-8";
		}
		else if (language.toLowerCase().contains("polish")){
			textFileNames.add("polish_text");
			textFileNames.add("polish_text1");
			charSet = "UTF-8";
		}

		FileInputStream fstream = null;
		try {
			fstream = new FileInputStream(
					"data//" + textFileNames.get(rnd.nextInt(textFileNames.size())) + ".txt");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		InputStreamReader isr = null;
		try {
			isr = new InputStreamReader(fstream, charSet);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		BufferedReader br = new BufferedReader(isr);
		rnd = new Random();
		rnd.setSeed(System.currentTimeMillis());
		try {
			br.skip(rnd.nextInt(100000));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		int nextChar;
		int wsMax = charLength;
		char workSentance[] = new char[wsMax];
		int wsIndex = 0;
		boolean haveSpace = false;

		wsIndex = 0;
		try {
			while (((nextChar = br.read()) != -1) && (wsIndex < wsMax)) {
				{
					if (haveSpace && (nextChar != 32)) {
						haveSpace = false;
					}
					if ((!((nextChar == 32) && haveSpace)) && (nextChar != 10)) {
						workSentance[wsIndex] = (char) nextChar;
						wsIndex++;
					}

					if (nextChar == 32) {
						haveSpace = true;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		String textString = new String(workSentance);
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		textString = textString.replaceAll("[^\\p{L}\\p{Zs}]", "");
		return(textString.trim());
	}
	// Test
	public static void utf8Test() {
//		System.out.println("Chinese: " + generateText(450, "chinese"));
//		System.out.println("******************************************************");
//		System.out.println("German: " + generateText(450, "german"));
//		System.out.println("******************************************************");
//		System.out.println("Japanese: " + generateText(450, "japanese"));
//		System.out.println("******************************************************");
//		System.out.println("English: " + generateText(450, "english"));
//		System.out.println("******************************************************");
//		System.out.println("Spanish: " + generateText(450, "spanish"));
//		System.out.println("******************************************************");
//		System.out.println("Hindi: " + generateText(450, "hindi"));
//		System.out.println("******************************************************");
//		System.out.println("Thai: " + generateText(450, "thai"));
//		System.out.println("******************************************************");
//		System.out.println("Tagalog: " + generateText(450, "filipino"));
//		System.out.println("******************************************************");
//		System.out.println("Vietnamese: " + generateText(450, "vietnamese"));
//		System.out.println("******************************************************");
//		System.out.println("Korean: " + generateText(450, "korean"));
//		System.out.println("******************************************************");
//		System.out.println("Turkish: " + generateText(450, "turkish"));
//		System.out.println("******************************************************");
//		System.out.println("Polish: " + generateText(450, "polish"));
	}

}
