package common;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * CkEditor is named after the ui control that allows users to enter text for IRC questions.
 *
 */
public class CkEditor {

	public WebDriver driver;
	public Utilities utils;
	Random rnd = new Random();
	String[] colors = {"#993399", "#993366", "#993333", "#996633", "#999933", "#669933", "#339933",
	"#339966", "#339999", "#336699", "#333399", "#663399", "#C247C2", "#D175D1", "#7C247"};

	public CkEditor(WebDriver driver){
		this.driver = driver;
		this.utils = new Utilities(driver);
	}

	public void enterText(String text){

		List<WebElement> iframeElements = driver.findElements(By.tagName("iframe"));
		WebElement frame = null;
		for (WebElement e: iframeElements){
			if (e.getAttribute("title").contains("Rich text editor, ext")){
				frame = e;
				break;
			}
		}
		driver.switchTo().frame(frame);
		WebElement body = driver.findElement(By.tagName("body"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].innerHTML = '" + text + "'", body);
		driver.switchTo().defaultContent();
		utils.wait(1000);
	}
	public void enterRandomText() {
		enterRandomText("english");
	}
	public void enterRandomText(String language) {
		String randomText = RandomUtf8Text.generateQuestion(language);
		enterText(randomText);
	}
	public void enterStyleText(String style, String text){
		String styleString = "<p style=\"" + style + "\">" + text + "</p>";
		enterText(styleString);	
	}
	public void enterMultipleLinesStyled(String args[]){
		Random rand = new Random();
		String headStyle = "font-family:Apple Chancery;color:" + colors[rand.nextInt(colors.length)] + ";font-size:26px;text-align:center;";
		String bodyStyle0 = "font-family:Caflisch Script;color:" + colors[rand.nextInt(colors.length)] + ";font-size:16px;";
		String bodyStyle1 = "font-family:Caflisch Script;color:" + colors[rand.nextInt(colors.length)] + ";font-size:14px;";
		String sendString = "";
		String tempString = "";
		tempString = "<p style=\"" + headStyle + "\" >" + args[0] + "</p>";	
		sendString += tempString;
		if (args.length > 1){
			tempString = "<p style=\"" + bodyStyle0 + "\" >" + args[1] + "</p>";	
			sendString += tempString;
		}
		if (args.length > 2){
			tempString = "<p style=\"" + bodyStyle1 + "\" >" + args[2] + "</p>";	
			sendString += tempString;
		}		enterText(sendString);
		utils.wait(1000);
	}
}
