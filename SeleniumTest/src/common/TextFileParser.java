package common;
import java.io.*;
import java.util.Random;

public class TextFileParser {

	public static void main(String[] args) throws IOException {

		FileInputStream fstream = null;
		try {
			fstream = new FileInputStream(
					"//Users//jbrennan//Documents//crime.txt");
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		Random rnd = new Random();
		rnd.setSeed(System.currentTimeMillis());
		br.skip(rnd.nextInt(10000));
		int nextChar;
		int wsMax = 120;
		char workSentance[] = new char[wsMax];
		int wsIndex = 0;
		boolean haveSpace = false;
		for (int r = 0; r < 2; r++) {
			wsIndex = 0;
			while (((nextChar = br.read()) != -1) && (wsIndex < wsMax)) {
				{
					if (haveSpace && (nextChar != 32)) {
						haveSpace = false;
					}
					if ((!((nextChar == 32) && haveSpace)) && (nextChar != 10)) {
						workSentance[wsIndex] = (char) nextChar;
						wsIndex++;
					}

					if (nextChar == 32) {
						haveSpace = true;
					}

				}
			}
			String bigQuestion = new String(workSentance);
			System.out.print(bigQuestion.trim());
		}
	}
}
