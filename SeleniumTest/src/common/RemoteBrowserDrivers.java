package common;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
/**
 * RemoteBrowserDrivers opens a browser by connecting to a Selenium Grid Hub
 * 
 * @author jbrennan
 *
 */
public class RemoteBrowserDrivers {

	public static RemoteWebDriver driver;
	public static Utilities utils;

	public static WebDriver startDriver(String browserName, String platform) {

		DesiredCapabilities capabilities;
		
		if (browserName.contains("explore")) {
			capabilities = new DesiredCapabilities(
					DesiredCapabilities.internetExplorer());
			capabilities.setBrowserName(browserName);
			capabilities.setCapability("EnableNativeEvents", true);
		} 
		
		else if (browserName.contains("irefox")) {
			capabilities = new DesiredCapabilities(DesiredCapabilities.firefox());
			capabilities.setBrowserName("firefox");
		} 
		
		else if (browserName.toLowerCase().contentEquals("safari")) {
			capabilities = new DesiredCapabilities(
					DesiredCapabilities.safari());
			capabilities.setBrowserName("Safari");
		}
		
		else if (browserName.toUpperCase().contains("CHROME")) {
			capabilities = new DesiredCapabilities(DesiredCapabilities.chrome());
			capabilities.setVersion("14");
			capabilities.setBrowserName("chrome");
		} 
		
		else {
			capabilities = new DesiredCapabilities(
					DesiredCapabilities.htmlUnit());
		}

		if (platform.contains("XP")){
			capabilities.setPlatform(Platform.XP);
			capabilities.setVersion("0");
		}
		
		else if (platform.contains("WIN8")){
			capabilities.setPlatform(Platform.WIN8);
			capabilities.setVersion("2");
		}
		
		else if (platform.contains("WIN7")){
			capabilities.setPlatform(Platform.WINDOWS);
			capabilities.setVersion("1");
		}
		
		else if (platform.contains("WINDOWS")){
			capabilities.setPlatform(Platform.WINDOWS);
			capabilities.setVersion("1");
		}
		
		else if (platform.contains("LINUX")){
			capabilities.setPlatform(Platform.LINUX);
			capabilities.setVersion("3");
		}
		
		else if (platform.contains("MAC")){
			capabilities.setPlatform(Platform.MAC);
			capabilities.setVersion("4");
		}

		try {
			driver = new RemoteWebDriver(
					new URL("http://localhost:4444/wd/hub"), capabilities);

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return driver;
	}

}
