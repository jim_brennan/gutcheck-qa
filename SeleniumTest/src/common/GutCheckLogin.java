/**  
* @author  Jim Brennan
* @version 1.0 
*/

package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**  
* A class containing methods that
* login to the Gutcheck UI.  
*/ 
	public class GutCheckLogin {
		
	/**
	 * Login as Admin takes the test from the login screen
	 * to inside the application. The credentials are hard-coded
	 * with Jim's credentials.
	 *
	 * @param driver - Login Methods require an instance of
	 * WebDriver in order to interact with the UI. WebDriver
	 * manages this such that all classes involved in a 
	 * test actually share a single static instance of 
	 * WebDriver 
	 */
		
	
	public static void Login(WebDriver driver, String userEmail, String userPassword){
		Utilities.staticWait(2000);
		driver.findElement(By.id("email-inputEl")).clear();
		driver.findElement(By.id("email-inputEl")).sendKeys(userEmail);
		driver.findElement(By.id("password-inputEl")).clear();;
		driver.findElement(By.id("password-inputEl")).sendKeys(userPassword);
		Utilities.staticWait(500);
		driver.findElement(By.id("loginBtn-btnIconEl")).click();
		Utilities.staticWait(3000);
	}
	public static void LoginAsAdmin(WebDriver driver){
		Login(driver, "jim@GutCheckIt.com", "jim");
	}
	public static void Logout(WebDriver driver){
		driver.findElement(By.id("menu-link-signout")).click();
		Utilities.staticWait(500);
	}
}
