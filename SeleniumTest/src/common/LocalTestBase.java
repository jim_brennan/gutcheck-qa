package common;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

public class LocalTestBase {

	protected static WebDriver driver;
	protected static String webSite = "http://dev.gutcheckit.com/";
	public Utilities utils;

	@BeforeClass
	public void classStartUp() {
		driver = BrowserDrivers.startDriver("firefox");
		utils = new Utilities(driver);
		driver.get(webSite);
	}

	@AfterClass
	protected void closeSession() {
		utils.wait(3000);
		if (driver != null) {
			driver.quit();
		}
	}
}