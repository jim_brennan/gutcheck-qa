package common;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

/**
 * GutCheckTestBase provides provides the basic @Before
 * and @After methods that start a WebDriver session and
 * quit the session at the end of the test. 
 */

public class GutCheckTestBase {

	public static WebDriver driver;
	public static Utilities utils;
	public static String webSite = "http://qa.gutcheckit.com/";
	public String userLogin = "";
	public static String serverLocation;
	public static String browserName;
	public static String platform;

	/**
	 * Close session - calls quit() on the driver.
	 */
	
	@AfterClass
	protected void closeSession() {
		if (driver != null) {
			driver.quit();
		}
	}

	/**
	 * The startSession method depends on the TestNg xml file to
	 * provide these parameters at runtime. startSession calls either
	 * BrowserDrivers or RemoteBrowserDrivers depending on the
	 * value of wdServer.
	 * 
	 * @param wdServer - "remote" or "local"
	 * @param browserName - Internet Explorer, Firefox, Chrome, Safari
	 * @param platform - WINDOWS, WIN8, XP, LINUX, MAC
	 */
	
	@BeforeClass
	@Parameters({ "serverLocation", "browserName", "platform" })
	public void startSession(String serverLocation, String browserName,
			String platform) {
		GutCheckTestBase.serverLocation = serverLocation;
		GutCheckTestBase.browserName = browserName;
		GutCheckTestBase.platform = platform;
		if (serverLocation.contentEquals("remote")) {
			driver = RemoteBrowserDrivers.startDriver(browserName, platform);
		} 
		else {
			driver = BrowserDrivers.startDriver(browserName);
		}
		utils = new Utilities(driver);
		utils.wait(1000);
		driver.get(webSite);
		utils.wait(2000);
	}

}
