package common.pageObjects;

import org.openqa.selenium.*;

import common.Utilities;

/**
 * IdiRespondentPages is a page object that holds methods used for
 * manipulating controls on the IDI Respondent pages.
 */
public class IdiRespondentPages {
	
		public WebDriver driver;
		public Utilities utils;
		/**
		 * CONSTRUCTOR: Instantiates a new IdiRespondentPages with it's own 
		 * reference to WebDriver.
		 * 
		 * @param driver - reference to driver from the calling script.
		 */ 
		public IdiRespondentPages(WebDriver driver){
			this.driver = driver;
			utils = new Utilities(driver);
		}
		/**
		 * Navigates to the Simulated Dashboard
		 */
		public void navigateToSimulatedDashboard(){
			driver.get("https://stage.gutcheckit.com/respondent/dashboard.jsp");
			utils.wait(2000);	
		}
		/**
		 * Clicks a project link on the Simulated Dashboard 
		 * page based on the partial link.
		 * 
		 * @param partialLinkText - The Project name without the random String
		 */
		public void clickProjectLink(String partialLinkText){
			driver.findElement(By.partialLinkText(partialLinkText)).click();
			utils.wait(3000);	
		}
		/**
		 * Clicks the "Get Started" button on the
		 * Respondent page.
		 */
		public void clickRespondentGetStarted(){
			utils.selectFromClassByAttribute("blue-big-button", "text", "GET STARTED").click();
			utils.wait(2000);	
		}
		/**
		 * Clicks the "Continue" (are you sure?) button
		 * on the respondent page.
		 */
		public void clickRespondentContinue(){
			driver.findElement(By.cssSelector("input[type=\"button\"]")).click();
			utils.wait(3000);	
		}
		/**
		 * Types the desired string into the Moderator chat input text
		 * area and clicks the button to send.
		 */
		public void respondentSendMessage(String respondentMessageText) {
			utils.wait(500);	
			driver.findElement(By.id("chatInputTextArea")).click();
			driver.findElement(By.id("chatInputTextArea")).clear();
			driver.findElement(By.id("chatInputTextArea")).sendKeys(respondentMessageText);
			driver.findElement(By.cssSelector("div.orange-big-button")).click();
			utils.wait(3000);
		}
	
		public void showMediaFullSize(int secondsToDisplay){
			driver.findElement(By.id("mediaImage")).click();
			utils.wait(4000); // time to bring up image
			utils.wait(secondsToDisplay * 1000 );
			driver.findElement(By.id("ux-lightbox-navClose")).click();
			utils.wait(2000);
		}
}
