package common.pageObjects;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.CkEditor;
import common.Utilities;


/**
 * IrcModeratorPages is a page object that holds methods used for
 * manipulating the Moderator controls of an IRC.
 */
public class IrcModeratorPages {
	
	public static WebDriver driver;
	public static Utilities utils;
	public CkEditor editor;
	
	//public String projectName = "Test";
	public String projectShortLink = "Test";
	Random rand = new Random();
	
	
	/**
	 * Returns an IrcModeratorPages page object
	 * 
	 * @param driver - reference to driver from the calling script.
	 */ 
	public IrcModeratorPages(WebDriver driver){
		IrcModeratorPages.driver = driver;
		utils = new Utilities(driver);
		editor = new CkEditor(driver);
	}
	
	//******************************** Moderator Dashboard *********************************************
	/**
	 * Clicks the link to take the moderator back to Dashboard.
	 */
	public void goToDashboard(){
		driver.findElement(By.id("menuDashboard")).click();
		utils.wait(500);
	}
	/**
	 * Clicks the orange "Create Project" button
	 * on the user dash board.
	 */
	public void ClickCreateProject(){
		driver.findElement(By.cssSelector("div.orange-big-button")).click();
		utils.wait(1000);	
	}
	/**
	 * Clears and sends keys to the Project Name input
	 * field.
	 */
	public void setProjectName(String projectNameString){
		driver.findElement(By.id("projectName")).clear();
		driver.findElement(By.id("projectName")).sendKeys(projectNameString);
		utils.wait(500);	
	}
	/**
	 *  @return projectName
	 */
//	public String getProjectName() {
//		//return projectName;
//	}
	/**
	 * Clicks the "Start Community" button on the create 
	 * project dialog.
	 */
	public void startCommunity(){
		//utils.selectFromClassByAttribute("orange-big-button", "text", "Start Community").click();
		driver.findElements(By.className("orange-big-button")).get(1).click();
		utils.wait(3000);
	}
	/**
	 * Clicks the "Use Your Own Respondent List" (Link) button to make it a link
	 * project.
	 */
	public void changeToLink(){
		String xPathExpression = "/html/body/div[2]/div[2]/div[1]/div[1]/div/div[2]/div[1]/div/div/div/table/tbody/tr[2]/td/div/div[2]/div/"
				+ "div/div/div/table/tbody/tr/td/div/div/form/table/tbody/tr[1]/td/div/div/div/fieldset/div/div/div/div[2]/"
				+ "div/div/table/tbody/tr/td[1]/div/img";
		driver.findElement(By.xpath(xPathExpression)).click();
		//driver.findElements(By.className("x-table-layout-cell")).get(0).click();
		utils.wait(2000);
	}
	/**
	 * Launches IRC (if no auto-launch) or sets
	 * the auto-launch date
	 */
	public void launchIrc(){
		utils.wait(500);
		List<WebElement> elements = driver.findElements(By.className("orange-big-button"));
		for (WebElement e: elements){
			if (e.getText().contains("Launch")){
				e.click();
			}
		}
		utils.wait(2000);
		utils.selectFromClassByAttribute("blue-big-button", "text", "OK").click();
		utils.wait(1000);
	}
	
//******************************** IRC Settings *********************************************
	/**
	 * Navigates to the IRC Settings page by finding elements
	 * by class "x-tab-strip-text" and clicking the 0 index element.
	 */
	public void goToSettingsPage(){
		utils.wait(1200);
		driver.findElements(By.className("x-tab-strip-text")).get(0).click();
		utils.wait(1200);
	}
	/**
	 * Sets number of respondents
	 * 
	 * @param desiredRespondents
	 */
	public void setDesiredRespondents(int desiredRespondents){
		driver.findElement(By.id("respondentNumberField")).clear();
		driver.findElement(By.id("respondentNumberField")).sendKeys(desiredRespondents + "\t");
	}
	/**
	 * Sets the number of discussion days in the project.
	 * 
	 * @param discussionDays
	 */
	public void setDiscussionDays(int discussionDays){
		driver.findElement(By.id("dayNumberField")).clear();
		driver.findElement(By.id("dayNumberField")).sendKeys(discussionDays + "\t");
	}
	/**
	 * Sets IRC Language
	 * 
	 * @param ircLanguage
	 */
	public void setIrcLanguage(String ircLanguage){
		utils.wait(500);
		driver.findElement(By.className("x-trigger-noedit")).click();
		utils.selectFromClassByAttribute("x-combo-list-item", "text", ircLanguage).click();
	}
	/**
	 * Finds the auto-launch checkbox by id, checks whether it's 
	 * checked, and clicks if it is not.
	 */
	public void checkIrcAutoLaunch(){
		if (!driver.findElement(By.id("isAutoLaunched")).isSelected()){
			driver.findElement(By.id("isAutoLaunched")).click();
		}
		utils.wait(500);
	}
	/**
	 * Sets the auto-launch date by finding the input by id
	 * and sending keys for date in the format MM/dd/yyyy.
	 * 
	 * @param launchDate
	 */
	public void setIrcAutoLaunchDate(String launchDate){
		driver.findElement(By.id("autoLaunchDate")).clear();
		driver.findElement(By.id("autoLaunchDate")).sendKeys(launchDate + "\t");
	}
	/**
	 * Sets the time zone based on the index of the area which
	 * is NOT available in the html. Use Selenium IDE to click the
	 * desired zone which produces the xpath.
	 */
	public void editTimeZone(int areaIndex){
		driver.findElement(By.id("projectEditTimeZone")).click();
		utils.wait(2000);
		driver.findElement(By.xpath("//map[@id='timezone-map']/area[" + areaIndex + "]")).click();
		utils.wait(1000);
		driver.findElements(By.className("x-tool")).get(1).click();
	}
	/**
	 * Get's the project's short link.
	 * 
	 * @return String value of project's short link
	 */
	public String getIrcShortLink(){
		return(driver.findElement(By.id("shortLink")).getText());
	}
	/**
	 * Get's the project's long link.
	 * 
	 * @return String value of project's long link
	 */
	public String getIrcLongLink(){
		return(driver.findElement(By.id("longLink")).getText());
	}
	/**
	 * Set's IRC into with the String provided.
	 * 
	 * @param ircInto
	 */
	public void setIrcIntroduction(String ircInto){
		driver.findElement(By.id("boardIntroduction")).clear();
		utils.wait(500);
		driver.findElement(By.id("boardIntroduction")).sendKeys(ircInto + "\t");
		utils.wait(500);
	}
	/**
	 * Sets Board Time
	 * 
	 * @param boardMinutes
	 */
	public void setBoardTime(int boardMinutes){
		driver.findElement(By.id("boardTime")).clear();
		driver.findElement(By.id("boardTime")).sendKeys(boardMinutes + "\t");
	}
	/** 
	 * Sets Click Count
	 * 
	 * @param clickCount
	 */
	public void setClickCount(int clickCount){
		driver.findElement(By.id("clickCount")).clear();
		driver.findElement(By.id("clickCount")).sendKeys(clickCount + "\t");
	}
	/**
	 * Selects an Answer Visibility radio button.
	 *   
	 * @param visibility - can be "None", "Private" or "Public"
	 */
	public void selectAnswerVisibility(String visibility){
		if (visibility.toUpperCase().equals("NONE")){
			driver.findElement(By.id("none-answer-visibility")).click();
		}
		else if (visibility.toUpperCase().equals("PRIVATE")){
			driver.findElement(By.id("private-answer-visibility")).click();
		}
		else if (visibility.toUpperCase().equals("PUBLIC")){
			driver.findElement(By.id("public-answer-visibility")).click();
		}
		utils.wait(500);
	}
	/**
	 * Selects a Respondent Self Identification radio button.
	 *   
	 * @param visibility - can be "Required", "Optional" or "None"
	 */
	public void selectRespondentSelfIdentification(String fbIdentification){
		if (fbIdentification.toUpperCase().equals("REQUIRED")){
			driver.findElement(By.id("fbreq-social-login-type")).click();
		}
		else if (fbIdentification.toUpperCase().equals("OPTIONAL")){
			driver.findElement(By.id("fbopt-social-login-type")).click();
		}
		else if (fbIdentification.toUpperCase().equals("NONE")){
			driver.findElement(By.id("nofb-social-login-type")).click();
		}
		if (driver.findElements(By.className("x-window")).size() > 0){
			utils.selectFromClassByAttribute("x-btn-mc", "text", "OK").click();
			}
		utils.wait(500);
	}
	/**
	 * Selects a Media Visibility radio button.
	 *   
	 * @param visibility - can be "Public" or "Private"
	 */
	public void selectMediaVisibility(String visibility){
		if (visibility.toUpperCase().equals("PUBLIC")){
			driver.findElement(By.id("public-media-visibility")).click();
		}
		else if (visibility.toUpperCase().equals("PRIVATE")){
			driver.findElement(By.id("private-media-visibility")).click();
		}
		utils.wait(500);
	}
	/**
	 * Sets the reward with a string that includes the
	 * currency symbol.
	 * 
	 * @param reward - example: "$10.00"
	 */
	public void setReward(String reward){
		driver.findElement(By.id("rewardAmount")).clear();
		driver.findElement(By.id("rewardAmount")).sendKeys(reward + "\t");	
	}
	/** 
	 * Sets the project's Short Description
	 * 
	 * @param description
	 */
	public void setProjectShortDescription(String description){
		driver.findElement(By.id("projectShortDescription")).clear();
		driver.findElement(By.id("projectShortDescription")).sendKeys(description + "\t");		
	}
	/**
	 * Sets the partial-success URL to a default
	 * value that will validate.
	 * 
	 */
	public void setPartialSuccessUrl(){
		String url = "http://10.0.1.81/partialSuccess.html";
		driver.findElement(By.id("projectPartialSuccessUrl")).clear();
		utils.wait(300);
		driver.findElement(By.id("projectPartialSuccessUrl")).sendKeys(url + "\t");
		utils.wait(300);
	}
	/**
	 * Sets the projectFail URL to a default
	 * value that will validate.
	 * 
	 */
	public void setProjectFailUrl(){
		String url = "http://10.0.1.81/projectFail.html";
		driver.findElement(By.id("projectFailUrl")).clear();
		utils.wait(300);
		driver.findElement(By.id("projectFailUrl")).sendKeys(url + "\t");
		utils.wait(300);
	}
	/**
	 * Sets the over-quota URL to a default
	 * value that will validate.
	 * 
	 */
	public void setOverQuotaUrl(){
		String url = "http://10.0.1.81/overQuota.html";
		driver.findElement(By.id("projectOverQuotaUrl")).clear();
		utils.wait(300);
		driver.findElement(By.id("projectOverQuotaUrl")).sendKeys(url + "\t");	
		utils.wait(300);
	}
	
	//******************************** IRC Screening *********************************************
	/**
	 * Navigates to the IRC Screening page by finding elements
	 * by class "x-tab-strip-text" and clicking the 1 index element.
	 */
	public void goToScreeningPage(){
		driver.findElements(By.className("x-tab-strip-text")).get(1).click();
		utils.wait(1000);
	}
	public void clickAddCustomQuestion(){
		driver.findElements(By.className("x-simplelink")).get(0).click();
	}
	
	
	
	//******************************** IRC Questions *********************************************
	/**
	 * Navigates to the IRC Questions page by finding elements
	 * by class "x-tab-strip-text" and clicking the 2 index element.
	 */
		public void goToQuestionsPage(){
			utils.wait(500);
			List<WebElement> xTabs = driver.findElements(By.className("x-tab-strip-text"));
			for (WebElement e: xTabs){
				if (e.getText().toUpperCase().equals("QUESTIONS")){
					e.click();
				}
			}
			utils.wait(1000);
		}

		public void clickAddQuestion(){
			//utils.selectFromClassByAttribute("x-btn-text", "text", "ADD QUESTION").click();
			
			utils.wait(1000);
		}
		public void enterQuestion(int dayNumber, String questionText) {		
			enterQuestion(dayNumber, "english", questionText);
			}
		
		public void enterQuestion(int dayNumber) {		
		enterQuestion(dayNumber, "english", "");
		}
		
		public void enterQuestion(int dayNumber, String language, String questionText) {		
			expandTheDay(dayNumber);
			List<WebElement> addQuestionButtons = driver.findElements(By.className("x-btn-text"));
			utils.wait(1200);
			addQuestionButtons.get(dayNumber).click();
			utils.wait(800);
			editor.enterRandomText(language);
			clickSubmit();
			utils.wait(1000);
		}

		public void expandTheDay(int dayNumber){
			List<WebElement> dayExpanders = driver.findElements(By.className("x-tool"));
			if (driver.findElement(By.id("dayGrid" + dayNumber)).getAttribute("class").contains("collapsed")){
				dayExpanders.get(dayNumber - 1).click();
			}
		}
		public void questionCancel(){	
			utils.selectFromClassByAttribute("x-btn-text", "text", "Cancel").click();
		}
		
		public void clickSubmit(){	
			utils.selectFromClassByAttribute("x-btn-text", "text", "Submit").click();
		}
		
		public String launchBoardWithDefaults() {
			createBoardWithDefaults();
			projectShortLink = getIrcShortLink();
			launchIrc();
			return projectShortLink;
		}
		public static void postAllQuestions() {

			Boolean foundOne = true;
			while (foundOne){
				foundOne = false;
				List<WebElement> elements = driver.findElements(By.className("blue-small-button"));
				for (int i = 0; i < elements.size(); i++){
					if (elements.get(i).getText().toUpperCase().equals("POST NOW")){
						foundOne = true;
						elements.get(i).click();
						utils.wait(1000);
						break;
					}
				}
			}
		}
		public String createBoardWithDefaults() {
			ClickCreateProject();
			String projectName = "Test Board " + rand.nextInt(10000);
			setProjectName(projectName);
			startCommunity();
			utils.wait(2000);
//			setPartialSuccessUrl();
//			setProjectFailUrl();
//			setOverQuotaUrl();
//			projectShortLink = getIrcShortLink();
			goToQuestionsPage();
			utils.wait(1000);
			enterQuestion(1);
			utils.wait(500);
			goToSettingsPage();
			return projectName;
		}
//		/*********************************************************************
//	 	Add Poll															*/
		public void clickAddPoll(){
			utils.selectFromClassByAttribute("blue-small-button", "text", "Add Poll").click();
			utils.wait(500);
		}
//	/** Clicking "Add Poll" brings up the Quick Poll dialog.				 */
		public void setAnswerType(String answerType){
			List<WebElement> radioButtons = driver.findElements(By.className("x-form-radio"));
			//Select Multiple Answers
			radioButtons.get(1).click();
		}
		public void clickAddAnswer(){
			utils.selectFromClassByAttribute("x-btn-text", "text", "Add Answer").click();
			utils.wait(500);
		}
		public void enterAnswer(String answerText){
			driver.findElement(By.id("answerText")).clear();
			driver.findElement(By.id("answerText")).sendKeys(answerText);
		}
		public void clickSavePollAnswer(){
			utils.selectFromClassByAttribute("x-btn-text", "text", "Save").click();
		}
		public void clickPollOk(){
			utils.selectFromClassByAttribute("x-btn-text", "text", "OK").click();
		}
//		public void clickCancel(){		
//			utils.selectFromClassByAttribute("x-btn-text", "text", "Cancel").click();
//		}
//		public void clickDelete(){
//			
//			// class = x-btn-text
//		}
	
//	/** *******************************************************************	
//	  	"Add Media" puts up a dialog with two buttons, one to add from computer
//	    and one to upload from the WEB. The dialog controls are grouped below.  */
//	    
	    public void clickAddMedia(){
	    	driver.findElement(By.cssSelector("td.blue-small-button")).click();
		}	
	   
//	    /** *******************************************************************
//		Upload from Computer														*/
		public void clickAddMediaUploadFromComputer(){
			utils.selectFromClassByAttribute("orange-big-button", "text", "Upload Images From Your Computer").click();
		}
//	/** Clicking "Upload from Computer" brings up file-picker dialog. 		*/
		public void enterAddNewFileDescription(String description){
			driver.findElement(By.id("mediaFileDescription")).clear();
			driver.findElement(By.id("mediaFileDescription")).sendKeys(description);
		}
//		public void enterNewFileDescription(){
//			
//		}
//		public void enterNewFilePath(){
//			
//		}
//		public void addNewFileBrowse(){
//			
//		}
//		public void addNewFileCancel(){
//			
//		}
//		public void addNewFileUpload(){
//		
//		}
//	/** *******************************************************************
//		Upload from Web														*/
//		public void clickAddMediaUploadFromWeb(){
//			
//		}
//	/** Clicking "Upload from Web" brings up URL dialog.					*/
//		public void enterNewUrlDescription(){
//			
//		}
//		public void enterNewUrlText(){
//			
//		}
//		public void clickAddNewUrlAttach(){
//			
//		}

	
}
