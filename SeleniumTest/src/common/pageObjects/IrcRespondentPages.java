package common.pageObjects;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.CkEditor;
import common.PopUps;
import common.RandomUtf8Text;
import common.Utilities;

/**
 * IrcRespondentPages is a page object that holds methods used for
 * manipulating controls on the IRC Respondent pages.
 */
public class IrcRespondentPages {
	
		public WebDriver driver;
		public CkEditor editor;
		String respondentFirstName = "tester";
		Random rand = new Random();
		
		public Utilities utils;
		/**
		 * CONSTRUCTOR: Instantiates a new IrcRespondentPages with it's own 
		 * reference to WebDriver.
		 * 
		 * @param driver - reference to driver from the calling script.
		 */ 
		public IrcRespondentPages(WebDriver driver){
			this.driver = driver;
			utils = new Utilities(driver);
			editor = new CkEditor(driver);
			
		}
		/**
		 * Navigates to the Simulated Dashboard
		 */
		public void navigateToSimulatedDashboard(){
			if (driver.getCurrentUrl().contains("stage")){
			driver.get("https://stage.gutcheckit.com/respondent/dashboard.jsp");
			} else {
			driver.get("https://dev.gutcheckit.com/respondent/dashboard.jsp");	
			}
			utils.wait(3000);	
		}
		/**
		 * Clicks a project link on the Simulated Dashboard 
		 * page based on the partial link.
		 * 
		 * @param partialLinkText - The Project name without the random String
		 */
		public void clickProjectLink(String partialLinkText){
			List<WebElement> elements = driver.findElements(By.partialLinkText(partialLinkText));
			elements.get(elements.size() -1).click();
			utils.wait(3000);	
		}
		public void clickProjectLink(String partialLinkText, int linkIndex){
			List<WebElement> elements = driver.findElements(By.partialLinkText(partialLinkText));
			elements.get(linkIndex).click();
			utils.wait(3000);	
		}
		/**
		 * Clicks the "JOIN MY COMMUNITY" button on the
		 * Respondent Welcome page.
		 */
		public void clickJoinMyCommunity(){
			utils.wait(1000);	
			utils.selectFromClassByAttribute("orange-big-button", "text", "JOIN MY COMMUNITY").click();
			utils.wait(2000);	
		}
		/**
		 * Clears and types text into the First Name field
		 * on the IRC Respondent Login respondent page.
		 * @param fNameString - Respondent's first name
		 */
		public void enterRepondentFirstName(String fNameString){
			respondentFirstName = fNameString;
			driver.findElement(By.id("firstName")).clear();
			driver.findElement(By.id("firstName")).sendKeys(fNameString);;
		}
		/**
		 * Clears and types the  first letter of the respondent's first
		 * name into the Last Initial field on the IRC Respondent
		 * Login page.
		 * 
		 *  @param - lastInitial
		 */
		public void enterRespondentLastInitial(){
			driver.findElement(By.id("lastName")).clear();
			driver.findElement(By.id("lastName")).sendKeys(respondentFirstName.substring(0, 1).toUpperCase());
		}
		/**
		 * Clears and types specified text into the Last Initial field
		 * on the IRC Respondent Login page.
		 * 
		 *  @param - lastInitial
		 */
		public void enterRespondentLastInitial(String lastInitial){
			driver.findElement(By.id("lastName")).clear();
			driver.findElement(By.id("lastName")).sendKeys(lastInitial);
		}
		/**
		 * Clears and types a hard-coded location into the Location field 
		 * on the IRC Respondent Login page.
		 */
		public void enterRespondentLocation(){
			driver.findElement(By.id("location")).clear();
			driver.findElement(By.id("location")).sendKeys("Raleigh, NC");
		}
		/**
		 * Clears and types text into the Location field on the IRC Respondent
		 * Login page.  Location must be like "Seattle, WA".
		 * 
		 * @param respondentLocation
		 */
		public void enterRespondentLocation(String respondentLocation){
			driver.findElement(By.id("location")).clear();
			driver.findElement(By.id("location")).sendKeys(respondentLocation);
		}
		/**
		 * Creates a Respondent Email string using the Respondent's First 
		 * Name and "@yahoo.com" and sends to the email input.
		 */
		public void enterRespondentEmail(){
			driver.findElement(By.id("email1")).sendKeys(respondentFirstName + "@yahoo.com");
		}
		/**
		 * Enters the respondent's email in the
		 * input field.
		 * 
		 * @param email
		 */
		public void enterRespondentEmail(String email){
			driver.findElement(By.id("email1")).sendKeys(email);
		}
		public void pickRandomAvatar(){
			Random rand = new Random();
			String avatarXPath = "/html/body/div/div[2]/div/div/div/div/div[2]/div/div/div/div[3]/div/div/table/tbody/tr[" + 
					(rand.nextInt(6) + 1) + "]/td[" + (rand.nextInt(6) + 1) + "]/div/a/img";
	        driver.findElement(By.xpath(avatarXPath)).click();	
		}
		/**
		 * Clicks the "Continue with Facebook" button
		 * on the IRC Respondent Login page.
		 */
		public void clickContinueWithFacebook(){
			utils.selectFromClassByAttribute("orange-big-button", "text", "Continue with Facebook").click();
			utils.wait(3000);	
		}
		/**
		 * Clicks the "Continue without Facebook" button
		 * on the IRC Respondent Login page.
		 */
		public void clickContinueWithoutFacebook(){
			utils.selectFromClassByAttribute("orange-big-button", "text", "Continue without Facebook").click();
			utils.wait(3000);	
		}
		/**
		 * Clicks the "Continue to Community" button
		 * on the IRC Respondent Login page.
		 */
		public void clickContinueToCommunity(){
			utils.selectFromClassByAttribute("orange-big-button", "text", "Continue to Community").click();
			utils.wait(2000);	
		}
		/**   Screening Questions Selectors  ****************************/
		
		/**
		 * Selects the value for Gender in the
		 * demographic screening page.
		 */
		public void selectRespondentGender(String gender){

			List<WebElement> elements = driver.findElements(By.name("genderGroup"));
			elements.get(0).click();
			utils.wait(500);
	
			if (gender.toUpperCase().contains("FEMALE")){
				driver.findElements(By.className("x-combo-list-item")).get(1).click();
			}
			else{
				driver.findElements(By.className("x-combo-list-item")).get(0).click();
			}
		}
		/**
		 * Selects the value for Household Income in the
		 * demographic screening page.
		 */
		public void selectRespondentIncome(String income){
			driver.findElement(By.id("incomeGroup")).click();
			utils.wait(500);
			List<WebElement> choices = driver.findElements(By.className("x-combo-list-item"));
			for (WebElement e: choices){
				if (e.getText().contains(income)){
					e.click();
				}
			}
		}
		/**
		 * Selects the value for Education in the
		 * demographic screening page.
		 */
		public void selectRespondentEducation(String education){
			driver.findElement(By.id("educationGroup")).click();
			utils.wait(500);
			List<WebElement> choices = driver.findElements(By.className("x-combo-list-item"));
			for (WebElement e: choices){
				if (e.getText().toUpperCase().contains(education.toUpperCase())){
					e.click();
				}
			}
		}
		/**
		 * Selects the value for Ethnicity in the
		 * demographic screening page.
		 */
		public void selectRespondentEthnicity(String ethnicity){
			driver.findElement(By.id("ethnicityGroup")).click();
			utils.wait(500);
			List<WebElement> choices = driver.findElements(By.className("x-combo-list-item"));
			for (WebElement e: choices){
				if (e.getText().toUpperCase().contains(ethnicity.toUpperCase())){
					e.click();
				}
			}
		}
		/**
		 * Selects the value for Marital Status in the
		 * demographic screening page.
		 */
		public void selectRespondentMaritalStatus(String maritalStatus){
			driver.findElement(By.id("maritalStatusGroup")).click();
			utils.wait(500);
			List<WebElement> choices = driver.findElements(By.className("x-combo-list-item"));
			for (WebElement e: choices){
				if (e.getText().toUpperCase().equals(maritalStatus.toUpperCase())){
					e.click();
				}
			}
		}
		/**
		 * Selects the value for Employment in the
		 * demographic screening page.
		 */
		public void selectRespondentEmployment(String employment){
			driver.findElement(By.id("employmentGroup")).click();
			utils.wait(500);
			List<WebElement> choices = driver.findElements(By.className("x-combo-list-item"));
			for (WebElement e: choices){
//				PopUps.displayPopUp("List item: " + e.getText() + "  Employment: " + employment +
//						"  \nList size: " + choices.size());
				if (e.getText().toUpperCase().contains(employment.toUpperCase())){
					e.click();
				}
			}
		}
		/**
		 * Selects the value for Children in the
		 * demographic screening page.
		 */
		public void selectRespondentChildren(String children, Object age, Object gender){
			driver.findElement(By.id("childrenGroup")).click();
			utils.wait(250);
			if (children.toUpperCase().contains("NO")){
				utils.selectFromClassByAttribute("x-combo-list-item", "Text", "No").click();
			}
			else{
				utils.selectFromClassByAttribute("x-combo-list-item", "Text", "Yes").click();
				driver.findElement(By.id("childAgeNumberField")).sendKeys(age.toString());
				String genderString = gender.toString();
				if (genderString.toUpperCase().contains("BOY")){
					driver.findElements(By.className("x-form-radio")).get(0).click();
				} 
				else{
					driver.findElements(By.className("x-form-radio")).get(1).click();
				}
			}
		}
		public void addSecondRespondentChild(String age, String gender){
			driver.findElement(By.id("addChildAnswer")).click();
			utils.wait(1000);
			driver.findElements(By.className("x-form-text")).get(8).clear();
			driver.findElements(By.className("x-form-text")).get(8).sendKeys(age);	
			utils.wait(1000);
			if (gender.toUpperCase().contains("BOY")){
				driver.findElements(By.className("x-form-radio")).get(2).click();
			} 
			else{
				driver.findElements(By.className("x-form-radio")).get(3).click();
			}
		}
		/**
		 * Selects the value for Zip Code in the
		 * demographic screening page.
		 */
		public void selectRespondentZipCode(String zipCode){
			driver.findElement(By.id("zipCodeField")).sendKeys(zipCode);
		}
		/**
		 * Clicks the "Next" button to progess
		 * past the demographic screening page.
		 */
		public void screeningPageClickNext(){
			utils.selectFromClassByAttribute("x-btn-text", "text", "Next").click();
		}
		/**
		 * Selects the value for Age in the
		 * demographic screening page.
		 */
		public void selectRespondentAge(String age){
			utils.selectFromClassByAttribute("x-form-text", "name", "ageField").sendKeys(age);
		}
		/**
		 * Clicks the "PROCEED TO QUESTION 1" button
		 * on the IRC Respondent Login page.
		 */
		public void clickProceedToQuestion1(){
			utils.wait(1000);
			utils.selectFromClassByAttribute("orange-big-button", "text", "PROCEED TO QUESTION 1").click();
			utils.wait(3000);
		}
		/**
		 * Clicks the "You Have Another Question To Answer" button
		 * on the IRC Respondent Answers page.
		 */
		public void clickYouHaveAnotherQuestion(){
			utils.selectFromClassByAttribute("orange-big-button", "text", "YOU HAVE ANOTHER QUESTION").click();
			utils.wait(3000);
		}
		/**
		 * @throws UnsupportedEncodingException 
		 * 
		 */
		public void enterQuestionResponse() {
			enterQuestionResponse("english");
		}
		/**
		 * Clears and sends auto-generated text to the answer field of
		 * the respondent's question page.
		 * @throws UnsupportedEncodingException 
		 */
		
		public void enterQuestionResponse(String language) {
			utils.wait(1000);
			driver.findElement(By.id("answerTextArea")).clear();
			driver.findElement(By.id("answerTextArea")).sendKeys(RandomUtf8Text.generateText(200, language));
			utils.wait(1000);
		}
		/**
		 * 
		 * 
		 */
		public void enterQuestionResponseText(String answerText) {
			utils.wait(1500);
			driver.findElement(By.id("answerTextArea")).clear();
			driver.findElement(By.id("answerTextArea")).sendKeys(answerText);
			utils.wait(1000);
		}
		/**
		 * 
		 * 
		 */
		public void enterIrcComment() {
			enterIrcComment("english");
		}
		/**
		 * Picks a random "Comment" link from among those visible and
		 * enters a random snippet of text.
		 */
		public void enterIrcComment(String language) {
			List<WebElement> elements = driver.findElements(By.linkText("Comment"));
			if (elements.size() > 0){
				int choosingComment = rand.nextInt(elements.size());
				elements.get(choosingComment).click();
			driver.findElement(By.tagName("textarea")).clear();
			driver.findElement(By.tagName("textarea")).sendKeys(RandomUtf8Text.generateText(rand.nextInt(150) + 70, language) + "\r");
			utils.wait(1000);
			}
		}
		/**
		 * Clicks the Submit button on the respondent's question page.
		 */
		public void clickSubmit(){
			utils.selectFromClassByAttribute("x-btn-text", "text", "SUBMIT").click();
			utils.wait(1000);
		}
		public void  selectRadioOrCheckBoxValues(String selections){
			if (selections != null){
				utils.wait(3000);
				List<WebElement> choices = driver.findElements(By.className("x-form-radio"));
				if (choices.size() < 1){
					choices = driver.findElements(By.className(" x-form-checkbox"));
				}
				String[] choicesToMark = selections.split(",");
				utils.wait(1000);
				for (int i = 0; i < choicesToMark.length; i++){
					choices.get(Integer.parseInt(choicesToMark[i]) - 1).click();
					utils.wait(500);
				}
				utils.wait(500);
			}
		
		}
		public void  selectRadioOrCheckBoxValue(int selection){
				utils.wait(1000);
				List<WebElement> choices = driver.findElements(By.className("x-form-radio"));
				if (choices.size() == 0){
					choices = driver.findElements(By.className("x-form-checkbox"));
				}
				utils.wait(1000);
				choices.get(selection - 1).click();
				utils.wait(500);
			
		
		}
		public void clickJoinMyGroup() {
			utils.wait(1000);
			utils.selectFromClassByAttribute("orange-big-button", "text", "JOIN MY GROUP").click();
			utils.wait(3000);
			
		}
		
}
