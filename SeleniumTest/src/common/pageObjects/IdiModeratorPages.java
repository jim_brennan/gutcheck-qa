package common.pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import common.Utilities;

/**
 * IdiModeratorPages is a page object that holds methods used for
 * manipulating the Moderator controls of an IDI.
 */
public class IdiModeratorPages {
	
	public WebDriver driver;
	public Utilities utils;
	/**
	 * Returns an IdiModeratorPages page object that contains
	 * methods to manipulate the controls on the Moderator pages
	 * of an IDI.
	 * 
	 * @param driver - reference to driver from the calling script.
	 */ 
	public IdiModeratorPages(WebDriver driver){
		this.driver = driver;
		utils = new Utilities(driver);
	}
	/**
	 * Clicks the link to take the moderator back to Dashboard.
	 */
	public void navigateToDashboard(){
		driver.findElement(By.linkText("Dashboard")).click();
	}
	/**
	 * Clicks the orange "Create Project" button
	 * on the user dash board.
	 */
	public void ClickCreateProject(){
		driver.findElement(By.cssSelector("div.orange-big-button")).click();
		utils.wait(1000);	
	}
	/**
	 * Clears and sends keys to the Project Name input
	 * field.
	 * 
	 *  @param projectName
	 */
	public void setProjectName(String projectName){
		driver.findElement(By.id("projectName")).clear();
		driver.findElement(By.id("projectName")).sendKeys(projectName);
		utils.wait(500);	
	}
	/**
	 * Clicks the "Start 1-on-1" button on the create 
	 * project dialog.
	 */
	public void startOneOnOne(){
		utils.selectFromClassByAttribute("orange-big-button", "text", "Start 1-on-1").click();
		utils.wait(3500);
	}
	/**
	 * 
	 */
	public void clickManageProject(){
		driver.findElement(By.linkText("MANAGE PROJECT")).click();
	}
	/**
	 * 
	 */
	public void clickInterviewPrep(){
		driver.findElement(By.linkText("INTERVIEW PREP")).click();
	}
	/**
	 * 
	 */
	public void clickInterviewNow(){
		driver.findElement(By.linkText("INTERVIEW NOW")).click();
		utils.wait(1500);	
	}
	/**
	 * Clicks the "Chat Now" button on the Manage
	 * page.
	 */
	public void manageProjectChatNow(){
		utils.wait(250);
		Boolean foundIt = false;
		int counter = 0;
		while (!foundIt && counter < 60){
			List<WebElement> elements = driver.findElements(By.className("blue-big-button"));
			if (elements.size() > 0){
				for (WebElement e: elements){
					if (e.getText().equals("CHAT NOW")){
						e.click();
						foundIt = true;
					}
				}
				utils.wait(1000);
				counter++;
			}
		}
		//utils.selectFromClassByAttribute("blue-big-button", "text", "CHAT NOW").click();
		utils.wait(1500);	
	}
	/**
	 * Clicks the "Start Recruitment" button on the 
	 * Manage page.
	 */
	public void startRecruitment(){
		utils.selectFromClassByAttribute("blue-small-button", "text", "START RECRUITMENT").click();
		utils.wait(500);	
	}
	/**
	 * Clicks the "CHAT NOW" button on the dialog that appears
	 * when a respondent joins the chat.
	 */
	public void chatNowWithResopndent(){
		utils.wait(1000);	
		driver.findElement(By.id("chatNow")).click();;
		utils.wait(1000);	
	}
	/**
	 * Types the desired string into the Moderator chat input text
	 * area and clicks the button to send.
	 */
	public void moderatorSendMessage(String moderatorMessageText) {
		driver.findElement(By.id("chatInputTextArea")).clear();
		driver.findElement(By.id("chatInputTextArea")).sendKeys(moderatorMessageText);
		driver.findElement(By.cssSelector("div.orange-big-button")).click();
		utils.wait(3000);
	}
	public void addMedia(String path, String description) {
		driver.findElement(By.id("showMediaLibrary")).click();
		driver.findElement(By.id("chatPrep-link-upload-image")).click();
		driver.findElement(By.id("showMediaLibrary")).click();
		driver.findElement(By.id("chatPrep-link-upload-image")).click();
		utils.selectFromClassByAttribute("orange-big-button", "text", "Upload Images From The Web").click();
		driver.findElement(By.id("mediaUrlDescription")).clear();
		driver.findElement(By.id("mediaUrlDescription")).sendKeys(description);
		driver.findElement(By.id("txtmediaUrl")).clear();
		driver.findElement(By.id("txtmediaUrl")).sendKeys(path);
		driver.findElement(By.cssSelector("#ext-comp-1022 > div.orange-small-button")).click();
		utils.wait(1000);	
	}
	/**
	 * 
	 */
	public void saveChatMedia() {
		utils.selectFromClassByAttribute("x-btn-text", "text", "Save").click();
	}
	/**
	 * 
	 */
	public void showMedia() {
		List<WebElement> elements = driver.findElements(By.className("x-tab-strip-text"));
		for (WebElement e: elements){
			if (e.getText().contains("Media")){
				e.click();
			}
		}
		driver.findElement(By.cssSelector("td.first > a > img")).click();
		utils.wait(500);	
		List<WebElement> elements1 = driver.findElements(By.className("x-btn-text"));
		for (WebElement e: elements1){
			if (e.getText().contains("Start Showing")){
				e.click();
			}
		}
		utils.wait(4000);
	}
	/**
	 * 
	 */
	public void stopShowingMedia() {
		List<WebElement> elements = driver.findElements(By.className("x-tab-strip-text"));
		for (WebElement e: elements){
			if (e.getText().contains("Media")){
				e.click();
			}
		}
		utils.wait(500);	
		List<WebElement> elements1 = driver.findElements(By.className("x-btn-text"));
		for (WebElement e: elements1){
			if (e.getText().contains("Stop Showing")){
				e.click();
			}
		}
		utils.wait(2000);
	}
	/**
	 * Clicks the "End Chat" button on the moderator's chat page.
	 */
	public void endChat(){
		driver.findElement(By.id("endChatButton")).click();
		
		utils.wait(500);
	}
	/**
	 * Clicks the "Save Interview" button on the moderator's 
	 * end-of-chat dialog. 
	 */
	public void saveInterview(){
		List<WebElement> elements = driver.findElement(By.className("x-window")).findElements(By.className("x-btn-text"));
		for (WebElement e : elements) {
			if (e.getText().contains("Save Interview")) {
				e.click();
			}
		}
		utils.wait(5000);
	}
	/**
	 * Clicks the "Discard Interview" button on the moderator's 
	 * end-of-chat dialog. 
	 */
	public void discardInterview(){
		List<WebElement> elements = driver.findElement(By.className("x-window")).findElements(By.className("x-btn-text"));
		for (WebElement e : elements) {
			if (e.getText().contains("Discard Interview")) {
				e.click();
			}
		}
		utils.wait(5000);
	}
	/**
	 * Clicks one of the five starts on the Chat Summary 
	 * feedback page. 
	 *
	 * @param starNumber -  1 - 5
	 */
	public void rateInterview(int starNumber){
		String numberOfStars = "";
		switch (starNumber){
		case 1: numberOfStars = "one-star";
		case 2: numberOfStars ="two-stars";
		case 3: numberOfStars = "three-stars";
		case 4: numberOfStars = "four-stars";
		case 5: numberOfStars = "five-stars";
		}
		driver.findElements(By.className("x-window")).get(1).findElement(By.className(numberOfStars)).click();
		
		utils.wait(4000);	
	}
	/**
	 * Controls on the Chat Summary - Next Step dialog
	 * @param nextStep - "guide", "transcripts", "new" or "screen"
	 */
	public void selectNextStep(String nextStep){
		String nextStepString = "";
		if (nextStep.toLowerCase().contains("guide")){
			nextStepString = "button-guide";
		}else if (nextStep.toLowerCase().contains("transcripts")){
			nextStepString = "button-transcripts";	
		}else if (nextStep.toLowerCase().contains("new")){
			nextStepString = "button-start";	
		}else if (nextStep.toLowerCase().contains("screen")){
			nextStepString = "button-screen";	
		}
		WebElement newChatButton = driver.findElement(By.className(nextStepString));
		Actions actions = new Actions(driver);
		actions.moveToElement(newChatButton).clickAndHold().release();
		Action doAction = actions.build();
		doAction.perform();	
		utils.wait(2000);
	}
	public void feedbackForDiscardedChat(String reason){
		String reasonString = "";
		if (reason.toLowerCase().contains("slow")){
			reasonString = "button-slow";
		}else if (reason.toLowerCase().contains("short")){
			reasonString = "button-short";	
		}else if (reason.toLowerCase().contains("wrong")){
			reasonString = "button-wrong";	
		}else if (reason.toLowerCase().contains("run")){
			reasonString = "button-run";	
		}
		WebElement newChatButton = driver.findElement(By.className(reasonString));
		Actions actions = new Actions(driver);
		actions.moveToElement(newChatButton).clickAndHold().release();
		Action doAction = actions.build();
		doAction.perform();	
		
		utils.wait(1000);	
	}


}
