package common.pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;






public class NewIrcDemoPages {
	
	protected WebDriver driver;
	public NewIrcDemoPages(WebDriver driver){
		this.driver = driver;
	}
	
	public void selectDemographicCriteri(String criteria){
		List<WebElement> demoChecks = driver.findElements(By.className("x-form-field"));
		if (criteria.contains("Country")){
			demoChecks.get(0).click();
		}
		if (criteria.contains("Language")){
			demoChecks.get(1).click();
		}
		if (criteria.contains("Age")){
			demoChecks.get(2).click();
		}
		if (criteria.contains("Gender")){
			demoChecks.get(3).click();
		}
		if (criteria.contains("Children")){
			demoChecks.get(4).click();
		}
		if (criteria.contains("Ethnicity")){
			demoChecks.get(5).click();
		}
		if (criteria.contains("Income")){
			demoChecks.get(6).click();
		}
		if (criteria.contains("Education")){
			demoChecks.get(7).click();
		}
		if (criteria.contains("Geography")){
			demoChecks.get(8).click();
		}
		if (criteria.contains("Employment")){
			demoChecks.get(9).click();
		}
		if (criteria.contains("Marital")){
			demoChecks.get(10).click();
		}
	}
	
	
	
	
	
	
}
