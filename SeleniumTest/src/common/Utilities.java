/**  
* @author  Jim Brennan
* @version 1.0 
*/
package common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * The Utilities Class provides access to some methods 
 * that support various UI actions/situations.
 */
public class Utilities {
	
	/**
	 * Utilities methods require an instance of
	 * WebDriver in order to interact with the UI. 
	 */ 
	public static WebDriver driver;
	/**
	 * CONSTRUCTOR: Instantiates a new utilities with it's own 
	 * reference to WebDriver.
	 * 
	 * @param driver - reference to driver from the calling script.
	 */ 
	public Utilities(WebDriver driver){
		Utilities.driver = driver;
	}
	/**
	 * The wait method is a simple timer that waits the
	 * specified number of milliseconds before returning 
	 * control to the test.
	 * 
	 * @param millis - milliseconds to wait
	 */
	public void wait(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	/**
	 * The static version of the wait method waits the specified number of 
	 * milliseconds before returning control to the test.
	 * 
	 * @param millis - milliseconds to wait
	 */
	public static void staticWait(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}
	/**
	 * selectFromClassByAttribute first gets a list of all the WebElements
	 * with the specified class. Then, it iterates through the 
	 * list to find <strong>the first element</strong> that has the specified 
	 * attribute with the desired value. If no elements are found with the right
	 * combination, the method returns <strong>null</strong>.   
	 * 
	 * @param elementClass - the className WebDriver uses to make 
	 * the initial list.
	 * 
	 * @param attribute - the attribute to be evaluated
	 * 
	 * @param value - the value of the attribute of the desired element
	 *           
	 * @return element - the web element with the desired attribute value or <strong>null</strong>
	 */
	public WebElement selectFromClassByAttribute(String elementClass,String attribute, String value){
		WebElement element = null;
		List<WebElement> elements = driver.findElements(By.className(elementClass));
		if (attribute.toLowerCase().equals("text")) {
			for (WebElement e : elements) {
				//System.out.println(e.getText());
				if (e.getText().contains(value)) {
					element = e;
				}
			}
		}
		else{
			for (WebElement e : elements) {
				if (e.getAttribute(attribute) != null) {
					if (e.getAttribute(attribute).equals(value)) {
						element = e;
					}
				}
			}
		}
		return element;
	}
	/**
	 * setWindowSizeAndPosition moves and sizes the browser window
	 * according to the parameters.
	 * 
	 * @param xPosition - The horizontal position of the top-left corner of the browser.
	 * 
	 * @param yPosition - The vertical position of the top-left corner of the browser.
	 * 
	 * @param xDimension - The desired width of the browser.
	 * 
	 * @param yDimension - The desired height of the browser.
- 	 */
	public void setWindowSizeAndPosition(int xPosition, int yPosition,
			int xDimension, int yDimension) {
		driver.manage().window().setPosition(new Point(xPosition, yPosition));
		driver.manage().window().setSize(new Dimension(xDimension, yDimension));
		wait(1000);	
	}
	public void highlightElement(WebElement element) {
	
		for (int i = 0; i < 2; i++) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			String styleString = element.getAttribute("style");
			js.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element,
					"color: yellow; background: yellow; border-color: yellow; background: yellow; background-color: yellow; background-color: yellow");
			wait(100);
			js.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, styleString);
		}
	}
}
