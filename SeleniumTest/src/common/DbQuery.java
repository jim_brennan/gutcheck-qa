/**  
* @author  Jim Brennan
* @version 1.0 
*/

package common;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class DbQuery consolidates code necessary to query 
 * or update a Postgresql database by passing-in a SQL Query string. 
 * <strong>Important:</strong> The queryDb method will not
 * work for updates, and the executeDb method will not work 
 * for queries. 
 */

public class DbQuery {

	/**
	 * Performs <strong>write</strong> operations on a 
	 * database.
	 *
	 * @param dbQueryString 
	 */
	
	public static void executeDb(String dbQueryString) {
		Connection c = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection(
					"jdbc:postgresql://stage.gutcheckit.com:5432/umod", "umod",
					"umod");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		Statement st;
		try {
			st = c.createStatement();
			st.execute(dbQueryString);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Performs <strong>read-only</strong> operations on a 
	 * database.
	 * 
	 * @param dbQueryString
	 *
	 * @return List<String> where each row returned by the query is one
	 * String in the List.
	 *
	 *	How to implement dbQuery:
	 *		
	 *  	String queryString = "SELECT project_id FROM project where project_name like 'Auto Test%'";
	 *   
	 *		List<String> results = DbQuery.queryDb(queryString);
	 *
	 *		int i = 0;
	 *		while (i < results.size()){
	 *		System.out.println(results.get(i));
	 *		
	 *		i++;
	 *		}
	 */
	public static List<String> queryDb(String dbQueryString) {

		Statement st = null;
		ResultSet rs = null;
		Connection c = null;
		List<String> results = new ArrayList<String>();

		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection(
					"jdbc:postgresql://stage.gutcheckit.com:5432/umod", "umod",
					"umod");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		try {
			st = c.createStatement();
			rs = st.executeQuery(dbQueryString);

			while (rs.next()) {

				results.add(rs.getString(1));

			}
			rs.close();
			st.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return (results);
	}

}
