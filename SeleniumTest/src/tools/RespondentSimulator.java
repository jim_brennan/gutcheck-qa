package tools;

import java.io.FileInputStream;


import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import common.BrowserDrivers;
import common.PopUps;
import common.RandomNames;
import common.RandomUtf8Text;
import common.SpreadsheetData;
import common.Utilities;
import common.pageObjects.IrcRespondentPages;

/** The respondent simulator is a Data Factory fixture that signs-in respondents to
 *  an IRG, reading the demographic and Custom Question responses from an Excel sheet 
 **/

public class RespondentSimulator {

	
	public WebDriver driver;
	public Utilities utils;
	public IrcRespondentPages respPages;
	public static Random rand;
	
	/** Demographic Info */
	public String respondentId;
	public String respondentAge;
	public String gender;
	public String employment;
	public String householdIncome;
	public String education;
	public String zipCode;
	public String ethnicity;
	public String maritalStatus;
	public String children;
	public String childsGender;
	public String childsAge;
	public String secondChildsGender;
	public String secondChildsAge;
	
	/** Custom Questions */	
	public String question1;
	public String question2;
	public String question3;
	public String question4;
	public String question5;
	public String question6;
	public String question7;
	public String projectLink = "";
	
	/** The data provider iterates through all of the rows in the sheet randomly. Each
	 *  row causes the test to run once, with all of the row values as parameters.
	 */
	@DataProvider(name = "dp")
	public static Iterator<Object[]> spreadsheetData() throws IOException {
		InputStream spreadsheet = new FileInputStream(
				"data//MikeTestRespondents.xls");
		Iterator<Object[]> objectArray = new SpreadsheetData(spreadsheet)
		.getData().iterator();
		return objectArray;
	}
//	public RespondentSimulator(){
//		
//	}

	/*
	 * These are the screening data mapped horizontally on the data sheet.  
	 */
	@Factory(dataProvider = "dp")
	public RespondentSimulator(
			String respondentId,
			String respondentAge,
			String gender,
			String employment,
			String householdIncome,
			String education,
			String zipCode,
			String ethnicity,
			String maritalStatus,
			String children,
			String childsGender,
			String childsAge,
			String secondChildsGender,
			String secondChildsAge,
			String question1,
			String question2,
			String question3,
			String question4,
			String question5,
			String question6,
			String question7)
	{
		this.respondentId = respondentId;
		this.respondentAge =  respondentAge;
		this.gender = gender;
		this.employment = employment;
		this.householdIncome = householdIncome;
		this.education = education;
		this.zipCode = zipCode;
		this.ethnicity = ethnicity;
		this.maritalStatus = maritalStatus;	
		this.children = children;
		this.childsGender = childsGender;
		this.childsAge = childsAge;
		this.secondChildsGender = secondChildsGender;
		this.secondChildsAge = secondChildsAge;
		this.question1 = question1;
		this.question2 = question2;
		this.question3 = question3;
		this.question4 = question4;
		this.question5 = question5;
		this.question6 = question6;
		this.question7 = question7;
	}

	/** This is where the work takes place. It has to be a TestNG test because that's how the data factory 
	 * is used. The important parameters here are accessWithSimDashOrLink, which can be set to 'link' or 
	 * 'simdash' depending on whether the project is link or panel. For link, the script concatenates the 
	 * link around the projectCode provided. For panel, the script uses the simulated simulated dashboard.
	 */
	@Test
	public  void joinIrg() {
	
		driver = BrowserDrivers.startDriver("Firefox");
		utils = new Utilities(driver);
		respPages = new IrcRespondentPages(driver);
		
		/** Set the type of project acccess */
		String accessWithSimDashOrLink = "link";
		
		boolean finished = false;

		/** For a simdash project, paste the project name below */
		if (accessWithSimDashOrLink.contentEquals("simdash")) {
			utils.wait(1000);
			driver.get("https://stage.gutcheckit.com/respondent/dashboard.jsp");
			utils.wait(4000);
			respPages.clickProjectLink("New Customs", 9);
			utils.wait(500);
		}
		
		/** For a link project, copy and paste just the projectCode -or- just the google link. */
		else{
			String baseUrl = "https://stage.gutcheckit.com/respondent/respondentEntry?provider=link&projectCode=";
			String projectCode = "bcd9067b-b6fe-4b91-a5b4-5c14664ebbb9";
			String langSetExtId = "&setLng=en&externalId=";
			//String projectLink = "http://goo.gl/0K3Qhs";
			String projectLink = baseUrl + projectCode + langSetExtId + respondentId ;
			System.out.println(projectLink);
			driver.get(projectLink);
			utils.wait(2000);
		}
		
		if ((!driver.getCurrentUrl().contains("overQuota")) && (driver.findElements(By.id("answerTextArea")).isEmpty())){

			respPages.clickJoinMyGroup();
			utils.wait(1000);
			//******************** Login Page **************
			String name = RandomNames.getRandomFirstName(gender);
			respPages.enterRepondentFirstName(name);
			respPages.enterRespondentLastInitial(name.substring(0, 1));
			respPages.enterRespondentEmail();
			respPages.pickRandomAvatar();
			
			if (accessWithSimDashOrLink.contentEquals("simdash")) {
			 respPages.clickContinueWithoutFacebook();
				
				utils.wait(500);
			}
			else{
			//respPages.clickContinueToCommunity();
			respPages.clickContinueWithoutFacebook();
			utils.wait(500);
			}

			//******************** Screening Page **************	
			if (checkforDemoField("ageField")){
				respPages.selectRespondentAge(respondentAge);
			}	
			utils.wait(1000);
			if (checkforDemoField("genderGroup")){
				respPages.selectRespondentGender(gender);
			}
			if (checkforDemoField("incomeGroup")){
				respPages.selectRespondentIncome(householdIncome);
			}
			if (checkforDemoField("educationGroup")){
				respPages.selectRespondentEducation(education);
			}
			if (checkforDemoField("ethnicityGroup")){
				respPages.selectRespondentEthnicity(ethnicity);
			}
			if (checkforDemoField("maritalStatusGroup")){
				respPages.selectRespondentMaritalStatus(maritalStatus);
			}
			if (checkforDemoField("zipCodeField")){
				respPages.selectRespondentZipCode(zipCode);
			}
			if (checkforDemoField("employmentGroup")){
				respPages.selectRespondentEmployment(employment);
			}
			if (checkforDemoField("childrenGroup")){
				respPages.selectRespondentChildren(children, childsAge, childsGender);
			}
			if (checkforDemoField("secondChildGroup")){
				respPages.addSecondRespondentChild(secondChildsAge, secondChildsGender);
			}

			utils.wait(500);
			if (!driver.findElements(By.id("demoQuestions-txt-about-you")).isEmpty()){
			respPages.screeningPageClickNext();
			utils.wait(1000);
			}
			//******************** Custom Questions ************** 
			String[] customQuestions = {question1, question2, question3, question4, question5, question6, question7};
			for (String s : customQuestions){
				findAndClickChoice(s);
			}		
			if (driver.getCurrentUrl().contains("areYouSure")){
				respPages.clickProceedToQuestion1();
			}	
			else{
				utils.wait(500);
				finished = true;
			}

		}
		while (!finished){
			utils.wait(1000);
			List<WebElement> pollAnswers = driver.findElements(By.className("x-form-radio"));
			rand = new Random();
			if (pollAnswers.size() > 0) {
				pollAnswers.get(rand.nextInt(pollAnswers.size() - 1)).click();
			}
			else{
				pollAnswers = driver.findElements(By.className("x-form-checkbox"));
				if (pollAnswers.size() > 0) {
					pollAnswers.get(rand.nextInt(pollAnswers.size() - 1)).click();
				}
			}
			utils.wait(500);
			
// Add Media to Question will be here
			
			utils.wait(500);
			rand = new Random();
			respPages.enterQuestionResponseText("I am a " + respondentAge +
					"-year-old " + ethnicity + " " + gender + ", " + employment + ", $" + householdIncome +
					", " + maritalStatus + ", " + education + ", " + zipCode + " Custom 1: " + question1 + " Custom 2: " + 
					question2 + "Custom 3: " + question3 + "      \n" 
					 + RandomUtf8Text.generateText(rand.nextInt(300) + 300));
			respPages.clickSubmit();
			utils.wait(3000);


			respPages.enterIrcComment();
//				driver.findElement(By.linkText("Comment")).click();
//				driver.findElement(By.tagName("textarea")).clear();
//				driver.findElement(By.tagName("textarea")).sendKeys(RandomUtf8Text.generateText(rand.nextInt(150) + 70, "english") + "\r");
				utils.wait(1000);
			
			
			if (driver.findElements(By.className("orange-big-button")).size() > 0){
				utils.wait(500);
				driver.findElement(By.className("orange-big-button")).click();
				utils.wait(1000);
			}
			else{
				finished = true;
				break;
			} 
		}
		utils.wait(500);
		driver.close();
		utils.wait(200);
		driver.quit();
		utils.wait(500);
	}


	public void findAndClickChoice(String s) {
		
		if ((driver.getCurrentUrl().contains("customQuestions"))){
				int labelIndex = 1;
				List<WebElement> choiceLabels = driver.findElements(By.className("x-form-cb-label"));
				for (WebElement e: choiceLabels){
					if(e.getText().equals(s)){
						respPages.selectRadioOrCheckBoxValues(labelIndex + "");
					}
					else{
						labelIndex++;
					}
				}
				respPages.screeningPageClickNext();
				utils.wait(1000);	
		}

	}
	private Boolean checkforDemoField(String demoField){
		Boolean isItThere = false;
		List<WebElement> elements = driver.findElements(By.className("x-form-text"));
		for (WebElement e : elements){
			if (e.getAttribute("name").contentEquals(demoField)){
				isItThere = true;
				break;
			}
		}
		return isItThere;
	}
}
