package tools;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import common.BrowserDrivers;
import common.CkEditor;
import common.GutCheckLogin;
import common.RandomUtf8Text;
import common.Utilities;
import common.pageObjects.IrcModeratorPages;
import common.pageObjects.NewIrcDemoPages;

public class IrcBoardBuilder {

	static String webSite = "http://qa.gutcheckit.com";
	protected static IrcModeratorPages modPages;
	protected static NewIrcDemoPages ircDemoPages;
	protected static Random rand = new Random();
	protected static Utilities utils;
	protected static String projectName = "Test Board " + rand.nextInt(10000);
	protected static String projectType= "panel";
	protected static boolean boardExists = false;
	public static WebDriver driver;
	public static CkEditor editor;
	protected static String boardLanguage = "english"; 
	
	
	public static void main(String[] args) {
		
		int discussionDays = 5;
		int numberOfQuestions = 7;

		RandomUtf8Text.utf8Test();
		driver = BrowserDrivers.startDriver("firefox");
		driver.get(webSite);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		modPages = new IrcModeratorPages(driver);
		ircDemoPages = new NewIrcDemoPages(driver);
		utils = new Utilities(driver);
		editor = new CkEditor(driver);
		GutCheckLogin.LoginAsAdmin(driver);

		if (boardExists){
			utils.selectFromClassByAttribute("x-tab-strip-text", "text", "Your Communities").click();
			driver.findElement(By.partialLinkText(projectName)).click();
			utils.wait(1000);
			// If there is screening - it goes here.
			modPages.goToQuestionsPage();
			utils.wait(3000);
		}
		else { // Create project from scratch 	
			modPages.ClickCreateProject();
			modPages.setProjectName(projectName);
			modPages.startCommunity();
			utils.wait(2500);
			if (projectType.equals("link")) {
				String xPathExpression = "/html/body/div[1]/div[2]/div[1]/div[1]/div/div[2]/div[1]/div/div/div/"
						+ "table/tbody/tr[2]/td/div/div[2]/div/div/div/div/table/tbody/tr/td/div/div/form/"
						+ "table/tbody/tr[1]/td/div/div/div/fieldset/div/div/div/div[2]/div/div/table/tbody/tr/td[1]/div/img";
				driver.findElement(By.xpath(xPathExpression)).click();
				utils.wait(2000);
			}
			modPages.setDiscussionDays(discussionDays);
			driver.findElement(By.className("cke_button_source")).click();
			utils.wait(1000);
			String ircIntro = "<font style=\"margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 28px; font-weight: bold; color: rgb(211, 65, 36); " + 
						      "font-weight: normal; text-transform: none !important; background: transparent;\">" + projectName + "</font></h1><br>" +
						      "<font-size=12px>" + RandomUtf8Text.generateText(30, boardLanguage) + "</font>";
			WebElement body = driver.findElement(By
					.id("cke_contents_boardIntroduction"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = '" + ircIntro + "'",
					body);
			modPages.clickSubmit();
			utils.wait(1000);
			driver.switchTo().defaultContent();
			if (projectType.equals("link")) {
				modPages.setPartialSuccessUrl();
				modPages.setProjectFailUrl();
				modPages.setOverQuotaUrl();
			}
			modPages.goToQuestionsPage();
			utils.wait(3000);
		}

		for (int i = 0; i < discussionDays; i++){
			for (int j = 0; j < numberOfQuestions; j++){
				utils.wait(2000);
				modPages.enterQuestion(i + 1, boardLanguage, "none");
				if (j % 2 == 0) {
					// Add Poll
					utils.wait(1500);
					modPages.clickAddPoll();
					utils.wait(500);
					for (int k = 1; k < 9; k++){
					modPages.clickAddAnswer();
					utils.wait(250);
					String pollAnswerText = " ";
					while (pollAnswerText.length() < 10){
						pollAnswerText = RandomUtf8Text.generateText(40, boardLanguage);
					}
					modPages.enterAnswer("D" + (i + 1) + " Q" + (j + 1) + " A" + k + " " + pollAnswerText + "\n");
					utils.wait(500);
					}
					modPages.clickPollOk();
				}
			}

		}
		utils.wait(1000);
		driver.quit();
	}
}

