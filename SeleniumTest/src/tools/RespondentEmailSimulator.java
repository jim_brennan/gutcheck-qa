package tools;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import common.BrowserDrivers;
import common.RandomNames;
import common.SpreadsheetData;
import common.Utilities;
import common.pageObjects.IrcRespondentPages;

public class RespondentEmailSimulator {
	
	public WebDriver driver;
	public Utilities utils;
	public IrcRespondentPages respPages;
	public String respondentEmail;
	public String projectName = "Copperfield 0";

	@DataProvider(name = "dp")
	public static Iterator<Object[]> spreadsheetData() throws IOException {
		InputStream spreadsheet = new FileInputStream(
				"data//RespondentEmail.xls");
		Iterator<Object[]> objectArray = new SpreadsheetData(spreadsheet)
				.getData().iterator();
		return objectArray;
	}

	@Factory(dataProvider = "dp")
	public RespondentEmailSimulator(
			String respondentEmail
			)
	{
		this.respondentEmail = respondentEmail;
	}
	
	@Test
	public  void joinIrc(){
		driver = BrowserDrivers.startDriver("Firefox");
		utils = new Utilities(driver);
		respPages = new IrcRespondentPages(driver);
		
		/* Access through Simulated Dashboard */
		driver.manage().deleteAllCookies();
		driver.get("https://dev.gutcheckit.com/respondent/dashboard.jsp");
		utils.wait(2000);
		respPages.clickProjectLink(projectName);
  		
 	
		/* Access with Project Link  */
//		driver.get(projectLink);
//		utils.wait(4000);
		
		respPages.clickJoinMyCommunity();
		
		/*                Login Page              */
		String name = RandomNames.getRandomFirstName();
		respPages.enterRepondentFirstName(name);
		respPages.enterRespondentLastInitial(name.substring(0, 1));
		respPages.enterRespondentLocation(name + ", CA");
		respPages.enterRespondentEmail(respondentEmail);
		respPages.pickRandomAvatar();
		utils.wait(500);
		respPages.clickContinueWithoutFacebook();
		utils.wait(500);

		if (driver.getCurrentUrl().contains("gutcheckit")){
			respPages.clickProceedToQuestion1();
			respPages.enterQuestionResponse();
			respPages.clickSubmit();
			utils.wait(500);	
		}
		utils.wait(500);
		driver.quit();	
	}
}
