package tools;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common.GutCheckTestBase;
import common.pageObjects.IrcRespondentPages;

public class GridRespondent extends GutCheckTestBase {

	protected static String[] respondentNames = { "Mathew", "Ryan", "Betty",
			"Jerry", "Jamie", "Linda", "Mike", "Danny", "Randall", "Tina",
			"Roxanne", "Steven", "Consuela", "George", "Larry", "Carl",
			"Betty", "Rhonda", "Marilyn", "Billy" };
	protected static IrcRespondentPages respPages;
	protected static Random rand = new Random();
	public static String respondentFirstName = "";

	@BeforeClass
	public static void classStartUp() {
		respPages = new IrcRespondentPages(driver);
		String baseUrl = "https://qa.gutcheckit.com/respondent/respondentEntry?provider=link&projectCode=";
		String projectCode = "1e7ace60-d9d5-4405-9f15-b7f03985475e";
		String langSetExtId = "&setLng=en&externalId=";
	    Random rand = new Random();
		String projectLink = baseUrl + projectCode + langSetExtId + rand.nextInt(10000);
		driver.get(projectLink);
		utils.wait(1000);	
	}

	@Test
	public void respondToIrc() {
		utils.wait(1500);
		respPages.clickJoinMyCommunity();
		utils.wait(4000);	
		respondentFirstName = respondentNames[rand.nextInt(respondentNames.length)];
		respPages.enterRepondentFirstName(respondentFirstName);
		respPages.enterRespondentLastInitial();
		respPages.enterRespondentLocation();
		respPages.enterRespondentEmail();
		respPages.clickContinueWithoutFacebook();
		utils.wait(5000);	
		respPages.clickProceedToQuestion1();
		utils.wait(1000);	
		respPages.enterQuestionResponse();
		respPages.clickSubmit();
		utils.wait(3000);
		Assert.assertTrue(driver
				.findElement(By.className("orange-big-nobutton")).getText()
				.contains("Thank you for completing all of today's questions."));
	}
}
